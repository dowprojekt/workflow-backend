/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URLEncoder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import ch.bfh.ictm.data.wf.model.Tag;
import ch.bfh.ictm.data.wf.rest.controller.ApplicationSettingsRestController;
import ch.bfh.ictm.data.wf.rest.test.config.Configuration;
import ch.bfh.ictm.data.wf.rest.util.HTTPRequest;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author vgj1
 *
 */
public class ApplicationSettingsRestControllerTest {
	private static final Logger logger = LogManager.getLogger(ApplicationSettingsRestControllerTest.class);
	
	@Test 
	public void readVersion() throws Exception {
		logger.debug("execute test");
		String requestUrl = Configuration.getURL(ApplicationSettingsRestController.PATH_VERSION);
		HttpURLConnection requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		logger.debug("content-Type of answer is " + requestConnection.getContentType());
		logger.debug("retrieved " + HTTPRequest.readResponse(requestConnection));
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		requestConnection.disconnect();
	}
	
	@Test 
	public void readGlobalSN() throws Exception {
		logger.debug("execute test");
		String requestUrl = Configuration.getURL(ApplicationSettingsRestController.PATH_SN);
		HttpURLConnection requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		logger.debug("content-Type of answer is " + requestConnection.getContentType());
		logger.debug("retrieved " + HTTPRequest.readResponse(requestConnection));
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		requestConnection.disconnect();
	}
	
	@Test @Ignore
	public void createTags() throws Exception {
		logger.debug("execute test");
		Tag customerData = new Tag("Customer Data");
		Tag financialData = new Tag("Financial Data");
		financialData.setParent(customerData);
		Tag stockExchangeData = new Tag("Stock Exchange Data");
		stockExchangeData.setParent(financialData);
		Tag taxData = new Tag("Tax Data");
		taxData.setParent(financialData);

		String requestUrl = Configuration.getURL(ApplicationSettingsRestController.PATH_TAGS);

		createTag(requestUrl, customerData);
		createTag(requestUrl, financialData);
		createTag(requestUrl, stockExchangeData);
		createTag(requestUrl, taxData);
	}

	private void createTag(String requestUrl, Tag tag) throws IOException, ProtocolException,
			JsonGenerationException, JsonMappingException {
		logger.debug("execute test");
		HttpURLConnection requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), tag);
		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
		logger.debug("tag was successfully created and can now be retrieved at "
				+ requestConnection.getHeaderField("Location"));
		requestConnection.disconnect();
	}

	@Test
	public void readTags() throws Exception {
		logger.debug("execute test");
		String requestUrl = Configuration.getURL(ApplicationSettingsRestController.PATH_TAGS);
		HttpURLConnection requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		logger.debug("content-Type of answer is " + requestConnection.getContentType());
		logger.debug("retrieved " + HTTPRequest.readResponse(requestConnection));
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		requestConnection.disconnect();
	}
	
	@Test 
	public void updateTag() throws Exception {
		logger.debug("execute test");
		createTags();

		Tag customerData = new Tag("Customer Data");
		Tag taxData = new Tag("Tax Data");
		taxData.setParent(customerData); // was "Financial Data" before

		String requestUrl = Configuration.getURL(ApplicationSettingsRestController.PATH_TAGS + "/" + URLEncoder.encode(taxData.getLabel(), "UTF-8"));
		HttpURLConnection requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), taxData);
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("tag was successfully updated and can now be retrieved at "
						+ requestConnection.getHeaderField("Location"));
		requestConnection.disconnect();
	}
	
	@Test 
	public void deleteTag() throws Exception {
		logger.debug("execute test");

		Tag newTag = new Tag("Tag to be deleted");
		String requestUrl = Configuration.getURL(ApplicationSettingsRestController.PATH_TAGS);
		createTag(requestUrl, newTag);

		requestUrl = Configuration.getURL(ApplicationSettingsRestController.PATH_TAGS + "/" + URLEncoder.encode(newTag.getLabel(), "UTF-8"));
		HttpURLConnection requestConnection = HTTPRequest.sendDELETE(requestUrl, Configuration.getSessionCookie());
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("tag was successfully deleted");
		requestConnection.disconnect();
	}
	
	private void createTagsLoop(int id, int nbr, long sleep) throws Exception {
		String requestUrl = Configuration.getURL(ApplicationSettingsRestController.PATH_TAGS);
		for (int i = 0; i < nbr; ++i) {
			createTag(requestUrl, new Tag("Tag-" + id + "-" + i));
			Thread.sleep(sleep);			
		}
	}
	
	public void createTagsSimultaneously() {
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				try {
					createTagsLoop(1, 10, 3);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		t1.start();
		Thread t2 = new Thread(new Runnable() {
			public void run() {
				try {
					createTagsLoop(2, 10, 2);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		t2.start();		 
	}

	public static void main(String[] args) {
		ApplicationSettingsRestControllerTest test = new ApplicationSettingsRestControllerTest();
		test.createTagsSimultaneously();
	}
}
