/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.test.config;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


public class Configuration {
	private static final Logger logger = LogManager.getLogger(Configuration.class);

	public static final String SERVER_URL = "http://localhost:8080/WorkflowBackend"; // for dedicated tomcat
//	public static final String SERVER_URL = "http://localhost:8080"; // for embedded tomcat
	
	public static final String limesConfig = "{\"uuid\":\"\",\"metric\":\"hausdorff(x.polygon, y.polygon)\",\"source\":{\"id\":\"linkedgeodata\",\"endpoint\":\"http://linkedgeodata.org/sparql\",\"graph\":null,\"var\":\"?x\",\"pagesize\":\"2000\",\"restriction\":\"?x a lgdo:RelayBox\",\"property\":[\"geom:geometry/geos:asWKT RENAME polygon\"],\"type\":null},\"target\":{\"id\":\"linkedgeodata\",\"endpoint\":\"http://linkedgeodata.org/sparql\",\"graph\":null,\"var\":\"?y\",\"pagesize\":\"2000\",\"restriction\":\"?y a lgdo:RelayBox\",\"property\":[\"geom:geometry/geos:asWKT RENAME polygon\"],\"type\":null},\"acceptance\":{\"threshold\":\"0.9\",\"relation\":\"lgdo:near\",\"file\":null},\"review\":{\"threshold\":\"0.5\",\"relation\":\"lgdo:near\",\"file\":null},\"execution\":\"Simple\",\"granularity\":null,\"output\":\"N3\",\"prefix\":[{\"label\":\"geom\",\"namespace\":\"http://geovocab.org/geometry#\"},{\"label\":\"geos\",\"namespace\":\"http://www.opengis.net/ont/geosparql#\"},{\"label\":\"lgdo\",\"namespace\":\"http://linkedgeodata.org/ontology/\"}]}";

	public static String getURL(String path) {
		logger.debug("using Server path " + SERVER_URL);
		return Configuration.SERVER_URL + "/" + path;
	}
	
	public static List<String> getSessionCookie() {
		List<String> cookies = new ArrayList<String>();
		cookies.add("sessionid=1");
		return cookies;
	}
}
