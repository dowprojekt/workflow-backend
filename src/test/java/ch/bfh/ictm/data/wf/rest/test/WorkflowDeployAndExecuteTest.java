/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.test;

import java.net.HttpURLConnection;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import ch.bfh.ictm.data.wf.model.Action;
import ch.bfh.ictm.data.wf.model.DataFunction;
import ch.bfh.ictm.data.wf.model.ExecutionSettings;
import ch.bfh.ictm.data.wf.model.ExecutionSettings.ErrorBehavior;
import ch.bfh.ictm.data.wf.model.Schedule;
import ch.bfh.ictm.data.wf.model.Step;
import ch.bfh.ictm.data.wf.model.Tag;
import ch.bfh.ictm.data.wf.model.Workflow;
import ch.bfh.ictm.data.wf.model.Workflow.Status;
import ch.bfh.ictm.data.wf.rest.controller.DataFunctionRestController;
import ch.bfh.ictm.data.wf.rest.controller.WorkflowRestController;
import ch.bfh.ictm.data.wf.rest.test.config.Configuration;
import ch.bfh.ictm.data.wf.rest.util.HTTPRequest;

/**
 * @author vgj1
 *
 */
public class WorkflowDeployAndExecuteTest {
	private static final Logger logger = LogManager.getLogger(WorkflowDeployAndExecuteTest.class);
	
	@Test 
	public void deployAndExecuteGoogle() throws Exception {
		logger.debug("execute test");
		// (1) create data function
		DataFunction dataFunction = new DataFunction();
		dataFunction.setName("google ping service");
		dataFunction.setDescription("google service");
		dataFunction.setType(DataFunction.Type.REST);
		dataFunction.setMethod(DataFunction.Method.GET);
		dataFunction.setUri("http://www.google.com");
		dataFunction.addTag(new Tag("Web Search"));
		
		String requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION);
		HttpURLConnection requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), dataFunction);
		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
		logger.debug("datafunction was successfully created and can now be retrieved at " + requestConnection.getHeaderField("Location"));
		requestConnection.disconnect();
		
		// (2) create workflow
		Workflow workflow = new Workflow();
		workflow.setName("google ping workflow");
		workflow.setTargetGraph("http://test.lindas-data.ch/resource/test01");
		workflow.setDescription("pings google");
		workflow.setStatus(Status.CONFIGURATION);
		workflow.setVersion("test version");
		workflow.setPublished(false);
		workflow.addTag(new Tag("Web Search"));
		
		Action action = new Action();
		action.setDataFunction(dataFunction);
		Step step = new Step();
		step.addAction(action);
		workflow.addStep(step);
		
		// execution settings
		ExecutionSettings executionSettings = new ExecutionSettings();
		executionSettings.setErrorBehavior(ErrorBehavior.STOP);
		executionSettings.setPriority(1);
		workflow.setExecutionSettings(executionSettings);

		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW);
		requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), workflow);
		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
		String workflowLocation = requestConnection.getHeaderField("Location");
		String workflowId = workflowLocation.substring(requestUrl.length()+1, workflowLocation.length());
		logger.debug("workflow with id "
						+ workflowId
						+ " was successfully created and can now be retrieved at "
						+ workflowLocation);
		requestConnection.disconnect();

		// (3) deploy workflow
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/deploy");
		requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), null);
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("workflow with id "
						+ workflowId + " was successfully deployed");
		requestConnection.disconnect();
		
		// (4) execute
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/executionstatus/execute");
		requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), null);
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("execution of workflow with id "
						+ workflowId + " was successfully started");
		requestConnection.disconnect();
	}
	
	@Test @Ignore
	public void deployExecuteStopDeleteGoogle() throws Exception {
		logger.debug("execute test");
		// (1) create data function
		DataFunction dataFunction = new DataFunction();
		dataFunction.setName("google ping service");
		dataFunction.setDescription("google service");
		dataFunction.setType(DataFunction.Type.REST);
		dataFunction.setMethod(DataFunction.Method.GET);
		dataFunction.setUri("http://www.google.com");
		dataFunction.addTag(new Tag("Web Search"));
		
		String requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION);
		HttpURLConnection requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), dataFunction);
		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
		logger.debug("datafunction was successfully created and can now be retrieved at " + requestConnection.getHeaderField("Location"));
		requestConnection.disconnect();
		
		// (2) create workflow
		Workflow workflow = new Workflow();
		workflow.setName("google ping workflow");
		workflow.setTargetGraph("http://test.lindas-data.ch/resource/test01");
		workflow.setDescription("pings google");
		workflow.setStatus(Status.CONFIGURATION);
		workflow.setVersion("test version");
		workflow.setPublished(false);
		workflow.addTag(new Tag("Web Search"));
		
		Action action = new Action();
		action.setDataFunction(dataFunction);
		Step step = new Step();
		step.addAction(action);
		workflow.addStep(step);
		
		// execution settings
		Schedule schedule = new Schedule();
		schedule.setStart("2016-02-05T05:20:00.000Z");
		schedule.setEnd("2016-02-05T05:30:00.000Z");
		ExecutionSettings executionSettings = new ExecutionSettings();
		executionSettings.setErrorBehavior(ErrorBehavior.STOP);
		executionSettings.setPriority(1);
		executionSettings.setSchedule(schedule);
		workflow.setExecutionSettings(executionSettings);

		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW);
		requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), workflow);
		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
		String workflowLocation = requestConnection.getHeaderField("Location");
		String workflowId = workflowLocation.substring(requestUrl.length()+1, workflowLocation.length());
		logger.debug("workflow with id "
						+ workflowId
						+ " was successfully created and can now be retrieved at "
						+ workflowLocation);
		requestConnection.disconnect();

		// (3) deploy workflow
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/deploy");
		requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), null);
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("workflow with id "
						+ workflowId + " was successfully deployed");
		requestConnection.disconnect();
		
		// (4) execute
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/executionstatus/execute");
		requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), null);
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("execution of workflow with id "
						+ workflowId + " was successfully started");
		requestConnection.disconnect();
		
		// (5) stop execution
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/executionstatus/stop");
		requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), null);
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("execution of workflow with id "
						+ workflowId + " was successfully stopped");
		requestConnection.disconnect();
		
		// (6) delete
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId);
		requestConnection = HTTPRequest.sendDELETE(requestUrl, Configuration.getSessionCookie());
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("workflow with id "
						+ workflowId + " was successfully deleted");
		requestConnection.disconnect();		
	}

	@Test @Ignore
	public void deployAndExecuteLindas() throws Exception {
		logger.debug("execute test");
		// (1) create data function
		DataFunction dataFunction = new DataFunction();
		dataFunction.setName("lindas config service");
		dataFunction.setDescription("lindas config service");
		dataFunction.setType(DataFunction.Type.REST);
		dataFunction.setMethod(DataFunction.Method.GET);
		dataFunction.setUri("http://lindas-data.ch/rest/config");
		dataFunction.addTag(new Tag("Lindas"));
		
		String requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION);
		HttpURLConnection requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), dataFunction);
		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
		logger.debug("datafunction was successfully created and can now be retrieved at " + requestConnection.getHeaderField("Location"));
		requestConnection.disconnect();
		
		// (2) create workflow
		Workflow workflow = new Workflow();
		workflow.setName("lindas config request workflow");
		workflow.setTargetGraph("http://lindas-data.ch");
		workflow.setDescription("requests config from lindas");
		workflow.setStatus(Status.CONFIGURATION);
		workflow.setVersion("test version");
		workflow.setPublished(false);
		workflow.addTag(new Tag("Lindas"));
		
		Action action = new Action();
		action.setDataFunction(dataFunction);
		Step step = new Step();
		step.addAction(action);
		workflow.addStep(step);
		
		// execution settings
		// none = immediate execution	
		
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW);
		requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), workflow);
		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
		String workflowLocation = requestConnection.getHeaderField("Location");
		String workflowId = workflowLocation.substring(requestUrl.length()+1, workflowLocation.length());
		logger.debug("workflow with id "
						+ workflowId
						+ " was successfully created and can now be retrieved at "
						+ workflowLocation);
		requestConnection.disconnect();

		// (3) deploy workflow
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/deploy");
		requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), null);
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("workflow with id "
						+ workflowId + " was successfully deployed");
		requestConnection.disconnect();
		
		// (4) execute
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/executionstatus/execute");
		requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), null);
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("execution of workflow with id "
						+ workflowId + " was successfully started");
		requestConnection.disconnect();
		
		// (5) get execution status
		Thread.sleep(10000);
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/executionstatus");
		requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("retrieved execution status: " + HTTPRequest.readResponse(requestConnection));
		requestConnection.disconnect();		

		// (6) execute 2nd time
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/executionstatus/execute");
		requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), null);
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("execution of workflow with id "
						+ workflowId + " was successfully started");
		requestConnection.disconnect();
		
		// (7) get execution status
		Thread.sleep(10000);
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/executionstatus");
		requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("retrieved execution status: " + HTTPRequest.readResponse(requestConnection));
		requestConnection.disconnect();		
	}
}