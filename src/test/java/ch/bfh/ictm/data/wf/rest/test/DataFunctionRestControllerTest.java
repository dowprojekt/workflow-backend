/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.test;

import java.net.HttpURLConnection;
import java.net.URLEncoder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import ch.bfh.ictm.data.wf.model.DataFunction;
import ch.bfh.ictm.data.wf.model.Parameter;
import ch.bfh.ictm.data.wf.model.Tag;
import ch.bfh.ictm.data.wf.rest.controller.DataFunctionRestController;
import ch.bfh.ictm.data.wf.rest.test.config.Configuration;
import ch.bfh.ictm.data.wf.rest.util.HTTPRequest;

/**
 * @author vgj1
 *
 */
public class DataFunctionRestControllerTest {
	private static final Logger logger = LogManager.getLogger(DataFunctionRestControllerTest.class);

	
	@Test
	public void deleteDataFunction() throws Exception {
		logger.debug("execute test");
		createDataFunctions();
		
		String requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION + "/1");
		HttpURLConnection requestConnection = HTTPRequest.sendDELETE(requestUrl, Configuration.getSessionCookie());
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("datafunction 1 successfully deleted");
		requestConnection.disconnect();
		
	}

	@Test
	public void createDataFunctions() throws Exception {
		logger.debug("execute test");
		for (int i = 1; i <= 5; ++i) {
			DataFunction dataFunction = new DataFunction();
			dataFunction.setName("CreateNewCustomer");
			dataFunction.setDescription("creates a new customer");
			dataFunction.setType(DataFunction.Type.WSDL);
			dataFunction.setUri("http://localhost:8080/createcustomer/" + i);
			dataFunction.addTag(new Tag("CRM"));
			dataFunction.addParameter(new Parameter("string", "firstname", "customer first name"));
			dataFunction.addParameter(new Parameter("string", "lastname", "customer last name"));
			
			String requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION);
			HttpURLConnection requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), dataFunction);
	
			Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
			logger.debug("datafunction was successfully created and can now be retrieved at " + requestConnection.getHeaderField("Location"));
			requestConnection.disconnect();
		}
	}

	@Test
	public void readDataFunction() throws Exception {
		logger.debug("execute test");
		String requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION + "/2");
		HttpURLConnection requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		logger.debug("content-Type of answer is " + requestConnection.getContentType());
		logger.debug("retrieved " + HTTPRequest.readResponse(requestConnection));
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		requestConnection.disconnect();
	}

	@Test
	public void readDataFunctions() throws Exception {
		logger.debug("execute test");
		String requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION);
		HttpURLConnection requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		logger.debug("content-Type of answer is " + requestConnection.getContentType());
		logger.debug("retrieved " + HTTPRequest.readResponse(requestConnection));
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		requestConnection.disconnect();
	}
	
	@Test 
	public void updateDataFunction() throws Exception {
		logger.debug("execute test");
		createDataFunctions();
		
		DataFunction dataFunction = new DataFunction();
		dataFunction.setName("UpdateNewCustomer");
		dataFunction.setDescription("updates a customer");
		dataFunction.setType(DataFunction.Type.WSDL);
		dataFunction.setUri("http://localhost:8080/updatecustomer/");
		dataFunction.addTag(new Tag("CRM"));
		dataFunction.addParameter(new Parameter("string", "firstname", "customer first name"));
		dataFunction.addParameter(new Parameter("string", "lastname", "customer last name"));

		String requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION + "/2");
		HttpURLConnection requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), dataFunction);

		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("datafunction was successfully updated and can now be retrieved at "
						+ requestConnection.getHeaderField("Location"));
		requestConnection.disconnect();
	}
	
	@Test
	public void lockDataFunction() throws Exception {
		logger.debug("execute test");
		// (1) create new dataFunction
		//configure data function
		DataFunction dataFunction = new DataFunction();
		dataFunction.setName("limes service");
		dataFunction.setDescription("limes service");
		dataFunction.setType(DataFunction.Type.REST);
		dataFunction.setMethod(DataFunction.Method.POST);
		dataFunction.setUri("http://localhost:8080/limes-service");
		dataFunction.addTag(new Tag("CRM"));
		dataFunction.addParameter(new Parameter("application/json", "body", URLEncoder.encode(Configuration.limesConfig, "utf-8")));
		
		String requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION);
		HttpURLConnection requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), dataFunction);
		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
		String dataFunctionLocation = requestConnection.getHeaderField("Location");
		String dataFunctionId = dataFunctionLocation.substring(requestUrl.length()+1, dataFunctionLocation.length());
		logger.debug("dataFunction with id "
						+ dataFunctionId
						+ " was successfully created and can now be retrieved at "
						+ dataFunctionLocation);
		requestConnection.disconnect();
		
		// (2) lock dataFunction
		requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION + "/" + dataFunctionId + "/lock");
		requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), null);
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("dataFunction with id " + dataFunctionId
				+ " was successfully locked");
		requestConnection.disconnect();
	
		// (3) retrieve dataFunction lock
		requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION + "/" + dataFunctionId + "/lock");
		requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		logger.debug("content-Type of answer is " + requestConnection.getContentType());
		logger.debug("retrieved " + HTTPRequest.readResponse(requestConnection));
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		requestConnection.disconnect();
				
		// (4) unlock dataFunction
		requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION + "/" + dataFunctionId + "/lock");
		requestConnection = HTTPRequest.sendDELETE(requestUrl, Configuration.getSessionCookie());
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("dataFunction with id " + dataFunctionId
				+ " was successfully unlocked");
		requestConnection.disconnect();

		// (5) retrieve dataFunction lock
		requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION + "/" + dataFunctionId + "/lock");
		requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		Assert.assertEquals(HttpURLConnection.HTTP_NO_CONTENT, requestConnection.getResponseCode());
		logger.debug("dataFunction with id " + dataFunctionId
				+ " is currently unlocked");
		requestConnection.disconnect();
	}
}