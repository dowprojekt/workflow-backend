/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.test;

import java.net.HttpURLConnection;
import java.net.URLEncoder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import ch.bfh.ictm.data.wf.model.Action;
import ch.bfh.ictm.data.wf.model.DataFunction;
import ch.bfh.ictm.data.wf.model.Parameter;
import ch.bfh.ictm.data.wf.model.Step;
import ch.bfh.ictm.data.wf.model.Tag;
import ch.bfh.ictm.data.wf.model.Workflow;
import ch.bfh.ictm.data.wf.model.Workflow.Status;
import ch.bfh.ictm.data.wf.rest.controller.DataFunctionRestController;
import ch.bfh.ictm.data.wf.rest.controller.WorkflowRestController;
import ch.bfh.ictm.data.wf.rest.test.config.Configuration;
import ch.bfh.ictm.data.wf.rest.util.HTTPRequest;

/**
 * @author vgj1
 *
 */
public class WorkflowRestControllerTest {
	private static final Logger logger = LogManager.getLogger(WorkflowRestControllerTest.class);

	@Test @Ignore
	public void createWorkflows() throws Exception {
		logger.debug("execute test");
		int number = 2;
		for (int i = 1; i <= number; ++i) {
			Workflow workflow = new Workflow();
			workflow.setName("CRM data mangling" + i);
			workflow.setDescription("crunches and munches data");
			workflow.setStatus(Status.CONFIGURATION);
			workflow.setVersion("test version");
			workflow.setPublished(false);
			workflow.addTag(new Tag("CRM"));
			workflow.addTag(new Tag("ETL"));
			
			Action action1 = new Action();
			action1.setDataFunction(createDataFunction(i + "1"));
			Action action2 = new Action();
			action2.setDataFunction(createDataFunction(i + "2"));
			Step step1 = new Step();
			step1.addAction(action1);
			Step step2 = new Step();
			step2.addAction(action2);
			
			workflow.addStep(step1);
			workflow.addStep(step2);

			String requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW);
			HttpURLConnection requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), workflow);
			
			Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
			logger.debug("workflow was successfully created and can now be retrieved at "
							+ requestConnection.getHeaderField("Location"));
			String workflowLocation = requestConnection.getHeaderField("Location");
			String workflowId = workflowLocation.substring(requestUrl.length()+1, workflowLocation.length());
			requestConnection.disconnect();
			
			requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/executionstatus");
			requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
			logger.debug("content-Type of answer is " + requestConnection.getContentType());
			logger.debug("retrieved  workflow executionstatus: " + HTTPRequest.readResponse(requestConnection));
			Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
			requestConnection.disconnect();
		}
	}

	public DataFunction createDataFunction(String suffix) throws Exception {
		logger.debug("execute test");
		DataFunction dataFunction = new DataFunction();
		dataFunction.setName("CreateNewCustomer" + suffix);
		dataFunction.setDescription("creates a new customer");
		dataFunction.setType(DataFunction.Type.WSDL);
		dataFunction.setUri("http://localhost:8080/createcustomer/");
		dataFunction.addTag(new Tag("CRM"));
		dataFunction.addParameter(new Parameter("string", "firstname", "customer first name"));
		dataFunction.addParameter(new Parameter("string", "lastname", "customer last name"));
		
		String requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION);
		HttpURLConnection requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), dataFunction);

		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
		logger.debug("datafunction was successfully created and can now be retrieved at " + requestConnection.getHeaderField("Location"));
		requestConnection.disconnect();
		
		return dataFunction;
	}
	
	@Test 
	public void readWorkflow() throws Exception {
		logger.debug("execute test");
		String requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/2");
		HttpURLConnection requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		logger.debug("content-Type of answer is " + requestConnection.getContentType());
		logger.debug("retrieved " + HTTPRequest.readResponse(requestConnection));
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		requestConnection.disconnect();
	}

	@Test 
	public void readWorkflows() throws Exception {
		logger.debug("execute test");
		String requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW);
		HttpURLConnection requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		logger.debug("content-Type of answer is " + requestConnection.getContentType());
		logger.debug("retrieved " + HTTPRequest.readResponse(requestConnection));
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		requestConnection.disconnect();
	}
	
	@Test
	public void deleteWorkflow() throws Exception {
		logger.debug("execute test");
		createWorkflows();
		
		String requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/1");
		HttpURLConnection requestConnection = HTTPRequest.sendDELETE(requestUrl, Configuration.getSessionCookie());
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("workflow 1 successfully deleted");
		requestConnection.disconnect();		
	}
	
	@Test @Ignore // cannot be tested via unit tests any more
	public void updateWorkflow() throws Exception {
		logger.debug("execute test");
		createWorkflows();
		
		Workflow workflow = new Workflow();
		workflow.setName("CRM data mangling Update");
		workflow.setDescription("crunches and munches data");
		workflow.setStatus(Status.CONFIGURATION);
		workflow.setVersion("test version");
		workflow.setPublished(false);
		workflow.addTag(new Tag("CRM update"));
		workflow.addTag(new Tag("ETL update"));
		
		Action action1 = new Action();
		action1.setDataFunction(createDataFunction("update1"));
		Action action2 = new Action();
		action2.setDataFunction(createDataFunction("update2"));
		Step step1 = new Step();
		step1.addAction(action1);
		Step step2 = new Step();
		step2.addAction(action2);
		
		workflow.addStep(step1);
		workflow.addStep(step2);

		String requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/2");
		HttpURLConnection requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), workflow);
		
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("workflow was successfully updated and can now be retrieved at "
						+ requestConnection.getHeaderField("Location"));
		requestConnection.disconnect();
	}
	
	@Test
	public void lockWorkflow() throws Exception {
		logger.debug("execute test");
		// (1) create new workflow
		//configure data function
		DataFunction dataFunction = new DataFunction();
		dataFunction.setName("limes service");
		dataFunction.setDescription("limes service");
		dataFunction.setType(DataFunction.Type.REST);
		dataFunction.setMethod(DataFunction.Method.POST);
		dataFunction.setUri("http://localhost:8080/limes-service");
		dataFunction.addTag(new Tag("CRM"));
		dataFunction.addParameter(new Parameter("application/json", "body", URLEncoder.encode(Configuration.limesConfig, "utf-8")));
		
		String requestUrl = Configuration.getURL(DataFunctionRestController.PATH_DATAFUNCTION);
		HttpURLConnection requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), dataFunction);
		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
		logger.debug("datafunction was successfully created and can now be retrieved at " + requestConnection.getHeaderField("Location"));
		requestConnection.disconnect();
		
		// configure workflow
		Workflow workflow = new Workflow();
		workflow.setName("CRM data mangling");
		workflow.setDescription("crunches and munches data");
		workflow.setStatus(Status.CONFIGURATION);
		workflow.setVersion("test version");
		workflow.setPublished(false);
		workflow.addTag(new Tag("CRM"));
		workflow.addTag(new Tag("ETL"));
		
		Action action = new Action();
		action.setDataFunction(dataFunction);
		Step step = new Step();
		step.addAction(action);
		workflow.addStep(step);

		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW);
		requestConnection = HTTPRequest.sendPOST(requestUrl, Configuration.getSessionCookie(), workflow);
		Assert.assertEquals(HttpURLConnection.HTTP_CREATED, requestConnection.getResponseCode());
		String workflowLocation = requestConnection.getHeaderField("Location");
		String workflowId = workflowLocation.substring(requestUrl.length()+1, workflowLocation.length());
		logger.debug("workflow with id "
						+ workflowId
						+ " was successfully created and can now be retrieved at "
						+ workflowLocation);
		requestConnection.disconnect();

		// (2) lock workflow
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/lock");
		requestConnection = HTTPRequest.sendPUT(requestUrl, Configuration.getSessionCookie(), null);
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("workflow with id " + workflowId
				+ " was successfully locked");
		requestConnection.disconnect();
	
		// (3) retrieve workflow lock
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/lock");
		requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		logger.debug("content-Type of answer is " + requestConnection.getContentType());
		logger.debug("retrieved " + HTTPRequest.readResponse(requestConnection));
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		requestConnection.disconnect();
				
		// (4) unlock workflow
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/lock");
		requestConnection = HTTPRequest.sendDELETE(requestUrl, Configuration.getSessionCookie());
		Assert.assertEquals(HttpURLConnection.HTTP_OK, requestConnection.getResponseCode());
		logger.debug("workflow with id " + workflowId
				+ " was successfully unlocked");
		requestConnection.disconnect();

		// (5) retrieve workflow lock
		requestUrl = Configuration.getURL(WorkflowRestController.PATH_WORKFLOW + "/" + workflowId + "/lock");
		requestConnection = HTTPRequest.sendGET(requestUrl, Configuration.getSessionCookie());
		Assert.assertEquals(HttpURLConnection.HTTP_NO_CONTENT, requestConnection.getResponseCode());
		logger.debug("workflow with id " + workflowId
				+ " is currently unlocked");
		requestConnection.disconnect();
	}
}