/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

/**
 * defines the error behavior, priority and schedule of a workflow. If schedule = NULL
 * the workflow is executed immediately at the spring batch backend, otherwise
 * the execution may be single time or repeated times (interval).
 * 
 * @author vgj1
 *
 */
@Entity
@Access(value=AccessType.FIELD)
public class ExecutionSettings extends ModelObject {
	public enum ErrorBehavior {
		
		/**
		 * stop on failure
		 */
		STOP(0), 
		
		/**
		 * ignore failure if non fatal
		 */
		IGNORE(1), 
		
		/**
		 * restart on failure
		 */
		RESTART(2);
		
		private int id;
		
		private ErrorBehavior(int id) {
			this.id = id;
		}
		
		public static ErrorBehavior getType(int id) {
			for (ErrorBehavior e : ErrorBehavior.values()) {
				if (e.getId() == id) {
					return e;
				}
			}
			return ErrorBehavior.values()[0];
		}
		
		public int getId() {
			return id;
		}
	}
	
	private int errorBehavior = ErrorBehavior.STOP.getId();

	private int priority = 0;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Schedule schedule = null;
	
	public ExecutionSettings() {
	}
	
	public ErrorBehavior getErrorBehavior() {
		return ErrorBehavior.getType(errorBehavior);
	}
	
	public void setErrorBehavior(ErrorBehavior errorBehavior) {
		this.errorBehavior = errorBehavior.getId();
	}
	
	public int getPriority() {
		return priority;
	}
	
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	public Schedule getSchedule() {
		return schedule;
	}
	
	public boolean hasSchedule() {
		if ((schedule == null) || (schedule.isManual())) {
			return false;
		}
		return true;
	}
	
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	
	@Override
	public void update(ModelObject update) {
		super.update(update);
		this.errorBehavior = ((ExecutionSettings)update).errorBehavior;
		this.priority = ((ExecutionSettings)update).priority;
		if (this.schedule == null) {
			this.schedule = ((ExecutionSettings)update).schedule; 
		} else {
			this.schedule.update(((ExecutionSettings)update).schedule);
		}
	}
}
