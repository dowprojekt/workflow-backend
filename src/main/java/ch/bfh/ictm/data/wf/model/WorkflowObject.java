/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;

import ch.bfh.ictm.data.wf.model.manager.TagManager;

@MappedSuperclass
@Access(value = AccessType.FIELD)
public abstract class WorkflowObject extends ModelObject {

	@ManyToMany(fetch = FetchType.EAGER) // JPA: not cascading since changes are managed manually
	private List<Tag> tags;
	
	
	public WorkflowObject() {
		tags = new ArrayList<Tag>();
	}
	
	public Iterator<Tag> getTagsIterator() {
		return tags.iterator();
	}
	
	public List<Tag> getTagsList() {
		List<Tag> copy = new ArrayList<Tag>();
		copy.addAll(tags);
		return copy;
	}
	
	public void replaceTags(List<Tag> tags) {
		this.tags.clear();
		this.tags.addAll(TagManager.getManager().getTags(tags)); // reuse any existing tags or create new ones
	}
	
	public void addTag(Tag tag) {
		if (!tags.contains(tag)) {
			tags.add(tag);
		}
	}
	
	public void removeTag(Tag tag) {
		tags.remove(tag);
	}
	
	@Override
	public void update(ModelObject update) {
		super.update(update);
		this.tags.clear();
		this.tags.addAll(TagManager.getManager().getTags(((WorkflowObject)update).tags)); // reuse any existing tags or create new ones
	}
}
