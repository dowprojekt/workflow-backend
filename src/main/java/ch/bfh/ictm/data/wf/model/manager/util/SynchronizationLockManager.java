/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model.manager.util;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.stereotype.Component;

import ch.bfh.ictm.data.wf.rest.util.SpringApplicationContext;

/**
 * Provides Id-based locks to serialize concurrent modifications of a workflow
 * object
 * 
 * @author vgj1
 */
//Spring
@Component
public class SynchronizationLockManager {
	public static SynchronizationLockManager getLockManager() {
		return SpringApplicationContext.getApplicationContext().getBean(SynchronizationLockManager.class);
	}

	private ConcurrentMap<String, Object> locks = new ConcurrentHashMap<String, Object>();
	
	public Object getLock(Class type, long id) {
		locks.putIfAbsent(type.getCanonicalName() + id, new Object());
		return locks.get(type.getCanonicalName() + id);
	}	
	
	public Object getLock(Class type, String label) {
		locks.putIfAbsent(type.getCanonicalName() + label, new Object());
		return locks.get(type.getCanonicalName() + label);
	}	
} 
