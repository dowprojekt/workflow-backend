/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Lob;

/**
 * One parameter for a particular data function. Type, name, and value must be defined
 * as required by the data function to be called.
 * 
 * @author vgj1
 */
@Entity
@Access(value=AccessType.FIELD)
public class Parameter extends ModelObject {
	private String type = "";

	private String name = "";
	
	@Lob // may be large
	private String description = "";
	
	@Lob // may  be large
	private String value = "";
	
	@Lob // may be large
	private String defaultValue = "";
	
	private boolean mandatory = true;
	
	private long dataFunctionId = -1;  // the data function this parameter belongs to
	
	private int sequence = -1; // the ordering number of this parameter in the overall parameter list for the data function it belongs to	
	
	public Parameter() {
	}
	
	public Parameter(String type, String name, String value) {
		this.type = type;
		this.name = name;
		this.value = value;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public boolean hasDefaultValue() {
		return (defaultValue != null);
	}
	
	public String getDefaultValue() {
		return defaultValue;
	}
	
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	public boolean isMandatory() {
		return mandatory;
	}
	
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	
	@Override
	public void update(ModelObject update) {
		super.update(update);
		this.defaultValue = ((Parameter)update).defaultValue;
		this.mandatory = ((Parameter)update).mandatory;
		this.name = ((Parameter)update).name;
		this.type = ((Parameter)update).type;
		this.value = ((Parameter)update).value;
		this.description = ((Parameter)update).description;
	}

	public long getDataFunctionId() {
		return dataFunctionId;
	}

	public void setDataFunctionId(long dataFunctionId) {
		this.dataFunctionId = dataFunctionId;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * checks whether this parameter (on its own) is ready for execution
	 * 
	 * @return true if ready for execution, false otherwise
	 */
	public boolean isReady() {
		if (mandatory) {
			if (value.isEmpty() && defaultValue.isEmpty()) {
				return false;
			}
		}
		
		if (type.isEmpty()) {
			return false;
		}

		return true; // everything ok
	}

}
