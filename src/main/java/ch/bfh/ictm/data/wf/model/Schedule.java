/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

/**
 * defines time of single or repeated (interval) execution of workflow.
 * 
 * @author vgj1
 */

@Entity
@Access(value=AccessType.FIELD)
public class Schedule extends ModelObject {
	  
	  private String start = ""; //in XSD DateTime Format: YYYY-MM-DDThh:mm:ss
	  private String end = ""; //in XSD DateTime Format: YYYY-MM-DDThh:mm:ss
	  private boolean intervalDay;
	  private boolean intervalWeek;
	  private boolean intervalMonth;

	public Schedule() {
	}
	
	@Override
	public void update(ModelObject update) {
		super.update(update);
		this.start = ((Schedule)update).start;
		this.end = ((Schedule)update).end;
		this.intervalDay = ((Schedule)update).intervalDay;
		this.intervalWeek = ((Schedule)update).intervalWeek;
		this.intervalMonth = ((Schedule)update).intervalMonth;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public boolean isIntervalDay() {
		return intervalDay;
	}

	public void setIntervalDay(boolean intervalDay) {
		this.intervalDay = intervalDay;
	}
	public boolean isIntervalWeek() {
		return intervalWeek;
	}

	public void setIntervalWeek(boolean intervalWeek) {
		this.intervalWeek = intervalWeek;
	}

	public boolean isIntervalMonth() {
		return intervalMonth;
	}

	public void setIntervalMonth(boolean intervalMonth) {
		this.intervalMonth = intervalMonth;
	}
	
	public boolean isManual() {
		return (start.isEmpty());
	}
}
