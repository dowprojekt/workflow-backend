/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model.manager;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ch.bfh.ictm.data.wf.database.DataFunctionRepository;
import ch.bfh.ictm.data.wf.database.UserRepository;
import ch.bfh.ictm.data.wf.model.ApplicationSettings;
import ch.bfh.ictm.data.wf.model.DataFunction;
import ch.bfh.ictm.data.wf.model.User;
import ch.bfh.ictm.data.wf.model.UserLock;
import ch.bfh.ictm.data.wf.model.manager.util.SynchronizationLockManager;
import ch.bfh.ictm.data.wf.rest.util.SpringApplicationContext;

/**
 * manage the creation, modification, deletion, and persistence of DataFunction
 * objects manipulated via the WFE frontend.
 * 
 * is implemented as singleton that is retrieved via the static method getManager().
 * 
 * @author vgj1
 *
 */

//Spring
@Component
//Spring JPA
@Transactional
public class DataFunctionManager {
	public static DataFunctionManager getManager() {
		return SpringApplicationContext.getApplicationContext().getBean(DataFunctionManager.class);
	}

	@Autowired
	private DataFunctionRepository dataFunctionRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	/**
	 * get data function by its id
	 * 
	 * @param id
	 * @return data function if found, null else
	 */
	public DataFunction getDataFunction(long id) {
		Optional<DataFunction> oDf = dataFunctionRepository.findById(id);
		return (oDf.isPresent() ? oDf.get() : null);
	}
	
	/**
	 * creates a data function from the (incomplete) object received from the
	 * WFE frontend.
	 * 
	 * @param dataFunction
	 * @return
	 */
	public synchronized DataFunction createDataFunction(DataFunction dataFunction) {
		if (dataFunction == null) return null;
		
		dataFunction.resetId();
		dataFunction.resetParameters();
		dataFunction.replaceTags(dataFunction.getTagsList()); 
		dataFunction.setSn(ApplicationSettings.getApplicationSettings().getNewSN());

		dataFunctionRepository.save(dataFunction);

		return dataFunction;
	}
	
	/**
	 * delete data function by its id
	 * 
	 * @param id
	 * @return true on success, else false
	 */
	public boolean deleteDataFunction(long id) {
		synchronized (SynchronizationLockManager.getLockManager().getLock(DataFunction.class, id)) {
			DataFunction dataFunction = getDataFunction(id);
			if (dataFunction == null) return false;
			
			dataFunction.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
			dataFunctionRepository.delete(dataFunction);
			
			return true;			
		}
	}

	/**
	 * updates an existing data function with the object received from the WFE frontend
	 * 
	 * @param id
	 * @param dataFunctionUpdate
	 * @return true on success, false else
	 */
	public boolean updateDataFunction(long id, DataFunction dataFunctionUpdate) {
		if (dataFunctionUpdate == null) return false;
		synchronized (SynchronizationLockManager.getLockManager().getLock(DataFunction.class, id)) {
			DataFunction dataFunction = getDataFunction(id);
			if (dataFunction == null)
				return false;

			dataFunction.update(dataFunctionUpdate);
			dataFunction.setSn(ApplicationSettings.getApplicationSettings()
					.getNewSN());
			dataFunctionRepository.save(dataFunction);

			return true;
		}
	}

	/**
	 * removes user lock from data function
	 * 
	 * @param id
	 * @return
	 */
	public boolean unlock(long id) {
		synchronized (SynchronizationLockManager.getLockManager().getLock(DataFunction.class, id)) {
			DataFunction dataFunction = getDataFunction(id);
			if (dataFunction == null) {
				return false;
			}

			// TODO here we may want to check whether the user is permitted to unlock the data function
			dataFunction.removeLock();
			dataFunction.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
			dataFunctionRepository.save(dataFunction);

			// successfully done
			return true;
		}
	}

	/**
	 * locks data function for the active user.
	 * 
	 * @param id
	 * @param sessionId
	 * @return
	 */
	public boolean lock(long id, String sessionId) {
		synchronized (SynchronizationLockManager.getLockManager().getLock(DataFunction.class, id)) {
			DataFunction dataFunction = getDataFunction(id);
			if (dataFunction == null) {
				return false;
			}
			
			if (dataFunction.isLocked()) {
				return false;
			}

			User user;
			Optional<User> oUser = userRepository.findBySession(sessionId);
			if (oUser.isPresent()) {
				user = oUser.get();
			} else {
				user = new User();
				user.setSession(sessionId);
				user.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
				userRepository.save(user);
				// TODO this should be an error once session management is implemented properly
			}

			UserLock userLock = new UserLock();
			userLock.setUser(user);
			userLock.setTime(System.currentTimeMillis());
			userLock.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
			dataFunction.setLock(userLock);
			dataFunction.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
			dataFunctionRepository.save(dataFunction);

			// successfully done
			return true;
		}
	}
}
