/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ch.bfh.ictm.data.wf.database.DataFunctionRepository;
import ch.bfh.ictm.data.wf.model.DataFunction;
import ch.bfh.ictm.data.wf.model.adapter.Adapter.ConvertWorkflowException;
import ch.bfh.ictm.data.wf.model.manager.DataFunctionManager;
import ch.bfh.ictm.data.wf.model.manager.WorkflowManager;
import ch.bfh.ictm.data.wf.rest.model.DataFunctionResource;
import ch.bfh.ictm.data.wf.rest.model.LockResource;
import ch.bfh.ictm.data.wf.rest.springbatchclient.SpringBatchClient.SpringBatchClientException;
import ch.bfh.ictm.data.wf.rest.util.HTTPResponse;

/**
 * Spring REST API configuration for /datafunctions - HATEOAS style
 * See the REST_API_Specification.txt for documentation.
 * 
 * @author vgj1
 *
 */
@RestController
@RequestMapping("/" + DataFunctionRestController.PATH_DATAFUNCTION)
public class DataFunctionRestController {
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public class DataFunctionException extends Exception {
		private static final long serialVersionUID = 1L;

		public DataFunctionException(String message) {
			super(message);
		}
	}
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public class DataFunctionNotFoundException extends Exception {
		private static final long serialVersionUID = 1L;

		public DataFunctionNotFoundException(long dataFunctionId) {
			super("could not find data function with id " + dataFunctionId);
		}
	}

	@ResponseStatus(HttpStatus.FORBIDDEN)
	public class DataFunctionLockException extends Exception {
		private static final long serialVersionUID = 1L;

		public DataFunctionLockException(long dataFunctionId) {
			super("data function with id " + dataFunctionId + " is currently locked");
		}
	}

	public static final String PATH_DATAFUNCTION = "datafunctions";
	
	private static final Logger logger = LogManager.getLogger(DataFunctionRestController.class);

	@Autowired
	private DataFunctionRepository dataFunctionRepository;
	
	public DataFunctionRestController() {
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	Resources<DataFunctionResource> readDataFunctions(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId) {
		logger.debug("received GET request to " + PATH_DATAFUNCTION + " by user with id " + sessionId);

		List<DataFunctionResource> datafunctions = dataFunctionRepository.findAll().stream().map(DataFunctionResource::new)
				.collect(Collectors.toList());
	
		logger.debug("data functions returned");
		return new Resources<DataFunctionResource>(datafunctions);
	}

	@RequestMapping(value = "/{dataFunctionId}", method = RequestMethod.GET)
	DataFunctionResource readDataFunctionById(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long dataFunctionId) 
			throws DataFunctionNotFoundException {
		logger.debug("received GET request to " + PATH_DATAFUNCTION + "/" + dataFunctionId + " by user with id " + sessionId);
		Optional<DataFunction> dataFunctionOptional = dataFunctionRepository.findById(dataFunctionId);
		if (dataFunctionOptional.isPresent()) {
			DataFunction dataFunction = dataFunctionOptional.get();
			logger.debug("data function returned");
		return new DataFunctionResource(dataFunction);
		} else {
			logger.error("data function " + dataFunctionId + " not found");
			throw new DataFunctionNotFoundException(dataFunctionId);
		}
	}	

	@RequestMapping(value = "", method = RequestMethod.POST)
	ResponseEntity<?> createDataFunction(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @RequestBody DataFunction dataFunction) {
		logger.debug("received POST request to " + PATH_DATAFUNCTION + " by user with id " + sessionId);
		logger.debug("data function to create: " + dataFunction);
		dataFunction = DataFunctionManager.getManager().createDataFunction(dataFunction);
		logger.debug("data function created: " + dataFunction);
		return HTTPResponse.createHTTPResponseCREATED(ServletUriComponentsBuilder.fromCurrentRequest().path("/{datafunctionId}")
				.buildAndExpand(dataFunction.getId()).toUri());
	}
	
	@RequestMapping(value = "/{dataFunctionId}", method = RequestMethod.PUT)
	ResponseEntity<?> updateDataFunction(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long dataFunctionId, @RequestBody DataFunction dataFunction) 
			throws DataFunctionNotFoundException, DataFunctionLockException {

		Optional<DataFunction> dataFunctionOptional = dataFunctionRepository.findById(dataFunctionId);
		if (!dataFunctionOptional.isPresent()) {
			logger.error("dataFunction " + dataFunctionId + " not found");
			throw new DataFunctionNotFoundException(dataFunctionId);
		}
		
		if (dataFunctionOptional.get().isLocked()) {
			logger.error("dataFunction " + dataFunctionId + " is locked");
			throw new DataFunctionLockException(dataFunctionId);
		}

		logger.debug("received PUT request to " + PATH_DATAFUNCTION + "/" + dataFunctionId + " by user with id " + sessionId);
		if (DataFunctionManager.getManager().updateDataFunction(dataFunctionId, dataFunction)) {		
			logger.debug("data function updated");
			return HTTPResponse.createHTTPResponseOK(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
		} else {
			logger.error("data function " + dataFunctionId + " not found");
			throw new DataFunctionNotFoundException(dataFunctionId);
		}
	}

	@RequestMapping(value = "/{dataFunctionId}", method = RequestMethod.DELETE)
	ResponseEntity<?> deleteDataFunction(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long dataFunctionId) 
			throws DataFunctionNotFoundException, DataFunctionLockException, DataFunctionException {
		logger.debug("received DELETE request to " + PATH_DATAFUNCTION + "/" + dataFunctionId + " by user with id " + sessionId);

		Optional<DataFunction> dataFunctionOptional = dataFunctionRepository.findById(dataFunctionId);
		if (!dataFunctionOptional.isPresent()) {
			logger.error("dataFunction " + dataFunctionId + " not found");
			throw new DataFunctionNotFoundException(dataFunctionId);
		}
		
		if (dataFunctionOptional.get().isLocked()) {
			logger.error("dataFunction " + dataFunctionId + " is locked");
			throw new DataFunctionLockException(dataFunctionId);
		}

		try {
			WorkflowManager.getManager().removeDataFunctionFromAllWorkflows(dataFunctionId);
		} catch (SpringBatchClientException | IOException | ConvertWorkflowException e) {
			logger.error("data function " + dataFunctionId + " delete failed, error is " + e.getMessage());
			throw new DataFunctionException(e.getMessage());
		}
		if (DataFunctionManager.getManager().deleteDataFunction(dataFunctionId)) {		
			logger.debug("data function deleted");
			return HTTPResponse.createHTTPResponseOK(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
		} else {
			logger.error("data function " + dataFunctionId + " not found");
			throw new DataFunctionNotFoundException(dataFunctionId);
		}
	}
	
	@RequestMapping(value = "/{dataFunctionId}/lock", method = RequestMethod.GET)
	ResponseEntity<?> readDataFunctionLock(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long dataFunctionId) 
			throws DataFunctionNotFoundException {
		logger.debug("received GET request to " + PATH_DATAFUNCTION + "/" + dataFunctionId + "/lock" + " by user with id " + sessionId);
		Optional<DataFunction> dataFunctionOptional = dataFunctionRepository.findById(dataFunctionId);
		if (dataFunctionOptional.isPresent()) {
			DataFunction dataFunction = dataFunctionOptional.get();
			if (dataFunction.isLocked()) {
				logger.debug("current lock returned");
				return new ResponseEntity<LockResource>(new LockResource(dataFunction.getLock()), null, HttpStatus.OK);
			} else {
				logger.debug("dataFunction is not locked, empty response");
				return HTTPResponse.createHTTPResponseNOCONTENT(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
			}
		} else {
			logger.error("data function " + dataFunctionId + " not found");
			throw new DataFunctionNotFoundException(dataFunctionId);
		}
	}
	
	@RequestMapping(value = "/{dataFunctionId}/lock", method = RequestMethod.DELETE)
	ResponseEntity<?> deleteDataFunctionLock(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long dataFunctionId) 
			throws DataFunctionNotFoundException, DataFunctionLockException {
		logger.debug("received DELETE request to " + PATH_DATAFUNCTION + "/" + dataFunctionId + "lock" + " by user with id " + sessionId);

		Optional<DataFunction> dataFunctionOptional = dataFunctionRepository.findById(dataFunctionId);
		if (!dataFunctionOptional.isPresent()) {
			logger.error("dataFunction " + dataFunctionId + " not found");
			throw new DataFunctionNotFoundException(dataFunctionId);
		}

		if (DataFunctionManager.getManager().unlock(dataFunctionId)) {		
			logger.debug("dataFunction lock deleted");
			return HTTPResponse.createHTTPResponseOK(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
		} else {
			logger.error("dataFunction " + dataFunctionId + " cannot be unlocked");
			throw new DataFunctionLockException(dataFunctionId);		
		}
	}	
	
	@RequestMapping(value = "/{dataFunctionId}/lock", method = RequestMethod.PUT)
	ResponseEntity<?> lockDataFunction(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long dataFunctionId) 
			throws DataFunctionNotFoundException, DataFunctionLockException {
		logger.debug("received PUT request to " + PATH_DATAFUNCTION + "/" + dataFunctionId + "/lock" + " by user with id " + sessionId);

		Optional<DataFunction> dataFunctionOptional = dataFunctionRepository.findById(dataFunctionId);
		if (!dataFunctionOptional.isPresent()) {
			logger.error("dataFunction " + dataFunctionId + " not found");
			throw new DataFunctionNotFoundException(dataFunctionId);
		}

		if (DataFunctionManager.getManager().lock(dataFunctionId, sessionId)) {		
			logger.debug("dataFunction locked successfully");
			return HTTPResponse.createHTTPResponseOK(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
		} else {
			logger.error("dataFunction " + dataFunctionId + " cannot be locked");
			throw new DataFunctionLockException(dataFunctionId);		
		}
	}
}