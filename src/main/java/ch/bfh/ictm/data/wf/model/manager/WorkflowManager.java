/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model.manager;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.persistence.Transient;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ch.bfh.ictm.data.wf.database.UserRepository;
import ch.bfh.ictm.data.wf.database.WorkflowRepository;
import ch.bfh.ictm.data.wf.model.ApplicationSettings;
import ch.bfh.ictm.data.wf.model.Execution;
import ch.bfh.ictm.data.wf.model.Execution.ExecutionResult;
import ch.bfh.ictm.data.wf.model.User;
import ch.bfh.ictm.data.wf.model.UserLock;
import ch.bfh.ictm.data.wf.model.Workflow;
import ch.bfh.ictm.data.wf.model.Workflow.Status;
import ch.bfh.ictm.data.wf.model.adapter.Adapter.ConvertWorkflowException;
import ch.bfh.ictm.data.wf.model.manager.util.SynchronizationLockManager;
import ch.bfh.ictm.data.wf.rest.springbatchclient.SpringBatchClient;
import ch.bfh.ictm.data.wf.rest.springbatchclient.SpringBatchClient.SpringBatchClientException;
import ch.bfh.ictm.data.wf.rest.util.SpringApplicationContext;

/**
 * manage the creation, modification, deletion, and persistence of Workflow
 * objects manipulated via the WFE frontend.
 * 
 * is implemented as singleton that is retrieved via the static method getManager().
 * 
 * @author vgj1
 *
 */

//Spring
@Component
//Spring JPA
@Transactional
public class WorkflowManager {
	@Transient
	private static final Logger logger = LogManager.getLogger(WorkflowManager.class);

	public static WorkflowManager getManager() {
		return SpringApplicationContext.getApplicationContext().getBean(WorkflowManager.class);
	}

	@Autowired
	private WorkflowRepository workflowRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	/**
	 * creates a workflow from the (incomplete) object received from the
	 * WFE frontend.
	 * 
	 * @param workflow
	 * @return
	 * @throws MalformedURLException
	 * @throws SpringBatchClientException
	 * @throws IOException
	 */
	public synchronized Workflow createWorkflow(Workflow workflow) 
			throws MalformedURLException, SpringBatchClientException, IOException {
		
		if (workflow == null) return null;
		
		workflow.resetId();
		workflow.removeLock();
		workflow.resetExecutionHistory();
		workflow.initExecutionSettings();
		workflow.replaceTags(workflow.getTagsList());
		workflow.replaceDataFunctions();
		workflow.setStatus(Workflow.Status.CONFIGURATION);
		workflow.checkStatus(); // maybe it is READY?
		saveWorkflow(workflow);

		return workflow;
	}
	
	/**
	 * deletes workflow by its id. also deletes it from the spring batch client
	 * (if it was deployed).
	 * 
	 * @param id
	 * @return true on success, else false 
	 * @throws MalformedURLException
	 * @throws SpringBatchClientException
	 * @throws IOException
	 */
	public boolean deleteWorkflow(long id) 
			throws MalformedURLException, SpringBatchClientException, IOException {

		Workflow workflow = getWorkflow(id);
		if (workflow == null)
			return false;

		if (workflow.isLocked()) {
			return false; // locked workflow cannot be deleted
		}

		// trigger operation at springbatch backend
		synchronized (SynchronizationLockManager.getLockManager().getLock(Workflow.class, id)) {
			logger.debug("workflow " + id + " sync locked");
			if (workflow.getStatus().equals(Workflow.Status.CONFIGURATION) || workflow.getStatus().equals(Workflow.Status.READY)) {
			} else {
				// need to clean up at spring batch backend too
				SpringBatchClient sbc = SpringBatchClient.getSpringBatchClient();
				try {
					sbc.startSession();
					sbc.stopWorkflow(workflow);
					sbc.deleteWorkflow(workflow);
					sbc.endSession();
				} catch (SpringBatchClientException | IOException e) {
					// ignore, we delete the workflow anyway
				}
			}

			workflow.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
			workflowRepository.delete(workflow);
			logger.debug("workflow " + id + " sync unlocked");
			return true;
		}
	}
	
	/**
	 * retrieves the workflow identified by its id. also updates the workflow status.
	 * 
	 * @param id
	 * @return Workflow if exists, null else
	 * @throws MalformedURLException
	 * @throws SpringBatchClientException
	 * @throws IOException
	 */
	public Workflow getWorkflow(long id) 
			throws MalformedURLException, SpringBatchClientException, IOException {

		Optional<Workflow> oWorkflow = workflowRepository.findById(id);
		if (oWorkflow.isPresent()) {
			Workflow workflow = oWorkflow.get();
			Status oldStatus = workflow.getStatus();
			synchronized (SynchronizationLockManager.getLockManager().getLock(Workflow.class, workflow.getId())) {
				logger.debug("workflow " + id + " sync locked");
				if (!workflow.checkStatus().equals(oldStatus)) {
					saveChangedStatus(workflow, oldStatus);					
				}
				logger.debug("workflow " + id + " sync unlocked");
			}
			return workflow;
		} else {
			return null;
		}
	}

	/**
	 * updates execution history and saves workflow
	 * 
	 * @param workflow
	 * @param oldStatus
	 */
	private void saveChangedStatus(Workflow workflow, Status oldStatus) {
		// has changed, need to persist change
		if (oldStatus.equals(Status.RUNNING)) {
			Execution latest = workflow.getActiveExecution();
			latest.setDuration(System.currentTimeMillis() - latest.getExecutiontime());
			if (workflow.getStatus().equals(Status.COMPLETED)) {
				latest.setExecutionResult(ExecutionResult.COMPLETED);
			} else if (workflow.getStatus().equals(Status.FAILED)) {
				latest.setExecutionResult(ExecutionResult.FAILED);						
			}
		}
		saveWorkflow(workflow);
	}
	
	/**
	 * is called when a data function is deleted in the WFE frontend to also
	 * remove this data function from all workflows (if used)
	 * 
	 * @param dataFunctionId
	 * @throws MalformedURLException
	 * @throws SpringBatchClientException
	 * @throws IOException
	 * @throws ConvertWorkflowException
	 */
	public void removeDataFunctionFromAllWorkflows(long dataFunctionId) 
			throws MalformedURLException, SpringBatchClientException, IOException, ConvertWorkflowException {

		Iterator<Workflow> i = workflowRepository.findAll().iterator();
		while (i.hasNext()) {
			Workflow workflow = i.next();
			synchronized (SynchronizationLockManager.getLockManager().getLock(Workflow.class, workflow.getId())) {
				logger.debug("workflow " + workflow.getId() + " sync locked");
				if (workflow.isLocked()) {
					continue; // locked workflow cannot be modified
				}

				switch (workflow.getStatus()) {
				case CONFIGURATION:
					workflow.removeDataFunctionFromAllSteps(dataFunctionId);
					workflow.checkStatus(); // maybe it is READY now
					saveWorkflow(workflow);
					break;
				case READY:
					workflow.removeDataFunctionFromAllSteps(dataFunctionId);
					workflow.checkStatus(); // maybe it is not READY anymore
					saveWorkflow(workflow);
					break;
				case QUEUED:
				case STOPPED:
				case FAILED:
				case COMPLETED:
					// workflow needs to be re-deployed
					SpringBatchClient sbc = SpringBatchClient.getSpringBatchClient();
					sbc.startSession();
					sbc.deleteWorkflow(workflow);
					workflow.removeDataFunctionFromAllSteps(dataFunctionId);
					workflow.checkStatus(); // maybe it is not READY anymore
					if (workflow.getStatus().equals(Workflow.Status.CONFIGURATION)) {
						// indeed is not READY anymore
					} else {
						sbc.deployWorkflow(workflow);
						workflow.setStatus(Status.QUEUED);
					}
					sbc.endSession();
					saveWorkflow(workflow);
					break;
				case RUNNING:
					// running workflow should not be modified
					break;
				}
				logger.debug("workflow " + workflow.getId() + " sync unlocked");
			}
		}
	}
	
	/**
	 * checks for status updates of running workflows at the spring batch client
	 * 
	 * @return true on success, else throws exception
	 * @throws MalformedURLException
	 * @throws SpringBatchClientException
	 * @throws IOException
	 */
	public boolean checkStatusFromAllRunningWorkflows() 
			throws MalformedURLException, SpringBatchClientException, IOException {

		List<Workflow> runningWorkflows = workflowRepository.findByStatus(Status.RUNNING.getId());
		if (runningWorkflows.isEmpty()) {
			logger.debug("no running workflows");
			return true; // nothing to do
		}
		
		SpringBatchClient sbc = SpringBatchClient.getSpringBatchClient();
		sbc.startSession();
		
		int nbrCompleted = 0;
		int nbrFailed = 0;
		Iterator<Workflow> i = runningWorkflows.iterator();
		while (i.hasNext()) {
			Workflow workflow = i.next();
			logger.debug("check workflow " + workflow);

			synchronized (SynchronizationLockManager.getLockManager().getLock(Workflow.class, workflow.getId())) {
				logger.debug("workflow " + workflow.getId() + " sync locked");
				if (!workflow.checkStatus4Running().equals(Status.RUNNING)) {
					// new status is FAILED or COMPLETED
					if (workflow.getStatus().equals(Status.FAILED)) {
						++nbrFailed;
					} else if (workflow.getStatus().equals(Status.COMPLETED)) {
						++nbrCompleted;						
					}
					logger.debug("workflow " + workflow.getId() + " is now " + workflow.getStatus() );
					saveChangedStatus(workflow, Status.RUNNING);
					break;						
				}
				logger.debug("workflow " + workflow.getId() + " sync unlocked");
			}
		}
		
		sbc.endSession();
		
		logger.debug("report: from previously " + runningWorkflows.size() + " running, "
				+ nbrCompleted + " have completed and  " 
				+ nbrFailed + " have failed");
		
		return true; // everything ok
	}
	
	/**
	 * checks for status updates of all workflows, including the status of
	 * deployed workflows at the spring batch client
	 * 
	 * @return on success, else throws an exception
	 * @throws MalformedURLException
	 * @throws SpringBatchClientException
	 * @throws IOException
	 */
	public boolean checkStatusFromAllWorkflows() 
			throws MalformedURLException, SpringBatchClientException, IOException {

		Iterator<Workflow> i = workflowRepository.findAll().iterator();
		while (i.hasNext()) {
			Workflow workflow = i.next();
		
			synchronized (SynchronizationLockManager.getLockManager().getLock(Workflow.class, workflow.getId())) {
				logger.debug("workflow " + workflow.getId() + " sync locked");
				if (workflow.isLocked()) {
					continue; // locked workflow cannot be modified
				}

				switch (workflow.getStatus()) {
				case CONFIGURATION:
					workflow.checkStatus(); // maybe it is READY now
					saveWorkflow(workflow);
					break;
				case READY:
					workflow.checkStatus(); // maybe it is not READY anymore
					saveWorkflow(workflow);
					break;
				case QUEUED:
				case STOPPED:
				case FAILED:
				case COMPLETED:
					// nothing to do
					break;
				case RUNNING:
					Status newStatus = workflow.checkStatus(); // maybe it is FAILED or COMPLETED now
					if (newStatus.equals(Status.COMPLETED)) {
						Execution latest = workflow.getActiveExecution();
						latest.setDuration(System.currentTimeMillis() - latest.getExecutiontime());
						latest.setExecutionResult(ExecutionResult.COMPLETED);
					} else if (newStatus.equals(Status.FAILED)) {
						Execution latest = workflow.getActiveExecution();
						latest.setDuration(System.currentTimeMillis() - latest.getExecutiontime());
						latest.setExecutionResult(ExecutionResult.FAILED);
					}
					saveWorkflow(workflow);
					break;
				}
				logger.debug("workflow " + workflow.getId() + " sync unlocked");
			}
		}
		
		return true; // everything ok
	}
	
	/**
	 * check status of workflow and save any changes
	 * 
	 * @param id
	 * @return
	 * @throws MalformedURLException
	 * @throws SpringBatchClientException
	 * @throws IOException
	 */
	public boolean checkWorkflowStatus(long id) 
			throws MalformedURLException, SpringBatchClientException, IOException {

		Workflow workflow = getWorkflow(id);
		if (workflow == null) {
			return false;
		}

		Status oldStatus = workflow.getStatus();
		synchronized (SynchronizationLockManager.getLockManager().getLock(Workflow.class, workflow.getId())) {
			logger.debug("workflow " + id + " sync locked");
			if (!oldStatus.equals(workflow.checkStatus())) {
				saveChangedStatus(workflow, oldStatus);				
			}
			logger.debug("workflow " + id + " sync unlocked");
		}
		return true;
	}

	/**
	 * increments the workflow's sn and saves it to the DB
	 * 
	 * @param workflow
	 */
	private void saveWorkflow(Workflow workflow) {
		workflow.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
		workflowRepository.saveAndFlush(workflow);
	}

	/**
	 * updates the workflow with id with the object received from the WFE
	 * frontend. also updates the corresponding worklow at the spring batch
	 * backend if required. A running workflow may not be updated (returns false).
	 * 
	 * @param id
	 * @param update
	 * @return true on success, false else
	 * @throws MalformedURLException
	 * @throws SpringBatchClientException
	 * @throws IOException
	 * @throws ConvertWorkflowException
	 */
	public boolean updateWorkflow(long id, Workflow update) 
			throws MalformedURLException, SpringBatchClientException, IOException, ConvertWorkflowException {

		if (update == null) return false;

		Workflow workflow = getWorkflow(id);
		if (workflow == null) {
			return false;
		}

		if (workflow.isLocked()) {
			return false; // locked workflow cannot be updated
		}

		if (workflow.getStatus().equals(Workflow.Status.RUNNING)) {
			return false; // running workflow should not be modified
		}

		synchronized (SynchronizationLockManager.getLockManager().getLock(Workflow.class, id)) {
			logger.debug("workflow " + id + " sync locked");
			workflow.update(update);

			switch (workflow.getStatus()) {
			case CONFIGURATION:
			case READY:
				workflow.checkStatus(); // maybe it is READY now
				saveWorkflow(workflow);
				break;
			case QUEUED:
			case STOPPED:
			case FAILED:
			case COMPLETED:
				// workflow needs to be re-deployed
				SpringBatchClient sbc = SpringBatchClient.getSpringBatchClient();
				sbc.startSession();
				sbc.deleteWorkflow(workflow);
				workflow.checkStatus(); // maybe it is not READY anymore
				if (workflow.getStatus().equals(Workflow.Status.CONFIGURATION)) {
					// indeed is not READY anymore
				} else {
					sbc.deployWorkflow(workflow);
					workflow.setStatus(Status.QUEUED);
				}
				sbc.endSession();
				saveWorkflow(workflow);
				break;
			case RUNNING:
				break; // is handled above
			}

			// successfully done
			logger.debug("workflow " + id + " sync unlocked");
			return true;
		}
	}
	
	/**
	 * (1) executes a workflow via operation "execute". in case it is currently
	 * running, the current execution is interrupted and the workflow restarted.
	 * 
	 * (2) stops a running workflow via operation "stop". Does nothing if the
	 * workflow is not running.
	 * 
	 * @param id
	 * @param operation
	 *            one of "execute" or "top".
	 * @return true if successful and false for errors: in case the workflow
	 *         does not exist or is locked (i.e., cannot be modified) or the
	 *         operation is not supported
	 * @throws MalformedURLException
	 * @throws SpringBatchClientException
	 * @throws IOException
	 * @throws ConvertWorkflowException
	 */
	public boolean changeWorkflowExecutionStatus(long id, String operation) 
			throws MalformedURLException, SpringBatchClientException, IOException, ConvertWorkflowException {

		Workflow workflow = getWorkflow(id);
		if (workflow == null) {
			return false;
		}

		if (workflow.isLocked()) {
			return false; // locked workflow cannot be executed or stopped
		}

		synchronized (SynchronizationLockManager.getLockManager().getLock(Workflow.class, id)) {
			logger.debug("workflow " + id + " sync locked");
			
			// 1. execute
			if (operation.equalsIgnoreCase("execute")) {
				if (workflow.getStatus().equals(Workflow.Status.CONFIGURATION)) {
					workflow.checkStatus(); // maybe it is READY now?
					if (workflow.getStatus().equals(Workflow.Status.CONFIGURATION)) {
						logger.debug("workflow " + id + " sync unlocked");
						return false; // still not READY, cannot be executed
					}
				} 

				// trigger operation at springbatch backend
				SpringBatchClient sbc = SpringBatchClient.getSpringBatchClient();
				sbc.startSession();

				if (workflow.getStatus().equals(Workflow.Status.READY)) {
					sbc.deployWorkflow(workflow); // need to deploy first
				}

				if (workflow.getStatus().equals(Workflow.Status.RUNNING)) {
					sbc.stopWorkflow(workflow); // need to restart
					Execution latest = workflow.getActiveExecution();
					latest.setDuration(System.currentTimeMillis() - latest.getExecutiontime());
					latest.setExecutionResult(ExecutionResult.STOPPED);
				}

				sbc.executeWorkflow(workflow);

				sbc.endSession();

				workflow.setStatus(Workflow.Status.RUNNING);
				
				Execution execution = new Execution();
				execution.setExecutionResult(ExecutionResult.RUNNING);
				execution.setExecutiontime(System.currentTimeMillis());
				workflow.addExecution(execution);
				
				saveWorkflow(workflow);

			// 2. stop
			} else if (operation.equalsIgnoreCase("stop")) {
				if (!workflow.getStatus().equals(Workflow.Status.RUNNING)) {
					// nothing to do
					logger.debug("workflow " + id + " sync unlocked");
					return true;
				}

				// trigger operation at springbatch backend
				SpringBatchClient sbc = SpringBatchClient.getSpringBatchClient();
				sbc.startSession();
				sbc.stopWorkflow(workflow);
				sbc.endSession();

				workflow.setStatus(Workflow.Status.STOPPED);
				Execution latest = workflow.getActiveExecution();
				latest.setDuration(System.currentTimeMillis() - latest.getExecutiontime());
				latest.setExecutionResult(ExecutionResult.STOPPED);
				saveWorkflow(workflow);
				
			// 3. NA
			} else {
				// operation not supported
				logger.debug("workflow " + id + " sync unlocked");
				return false;
			}

			// successfully done
			logger.debug("workflow " + id + " sync unlocked");
			return true;
		}
	}

	/**
	 * deploys the workflow with the given id to the spring batch backend.
	 * 
	 * @param id
	 * @return true on success, false else
	 * @throws MalformedURLException
	 * @throws SpringBatchClientException
	 * @throws IOException
	 * @throws ConvertWorkflowException
	 */
	public boolean deployWorkflow(long id) 
			throws MalformedURLException, SpringBatchClientException, IOException, ConvertWorkflowException {

		Workflow workflow = getWorkflow(id);
		if (workflow == null) {
			return false;
		}

		if (workflow.isLocked()) {
			return false; // locked workflow cannot be deployed
		}

		synchronized (SynchronizationLockManager.getLockManager().getLock(Workflow.class, id)) {
			logger.debug("workflow " + id + " sync locked");
			if (workflow.getStatus().equals(Workflow.Status.CONFIGURATION)) {
				workflow.checkStatus(); // maybe it is READY now?
				if (workflow.getStatus().equals(Workflow.Status.CONFIGURATION)) {
					logger.debug("workflow " + id + " sync unlocked");
					return false; // still not READY, cannot be deployed
				}
			} 
			// trigger operation at springbatch backend
			SpringBatchClient sbc = SpringBatchClient.getSpringBatchClient();
			sbc.startSession();

			if (workflow.getStatus().equals(Workflow.Status.READY)) {
				sbc.deployWorkflow(workflow);
			} else {
				// need to clean up at spring batch backend before we can redeploy
				if (workflow.getStatus().equals(Workflow.Status.RUNNING)) {
					Execution latest = workflow.getActiveExecution();
					latest.setDuration(System.currentTimeMillis() - latest.getExecutiontime());
					latest.setExecutionResult(ExecutionResult.STOPPED);
				}
				sbc.stopWorkflow(workflow);
				sbc.deleteWorkflow(workflow);
				sbc.deployWorkflow(workflow);
			}	

			sbc.endSession();

			workflow.setStatus(Workflow.Status.QUEUED);
			saveWorkflow(workflow);

			// successfully done
			logger.debug("workflow " + id + " sync unlocked");
			return true;
		}
	}

	/**
	 * removes user lock for the workflow with id
	 * 
	 * @param id
	 * @return
	 * @throws MalformedURLException
	 * @throws SpringBatchClientException
	 * @throws IOException
	 */
	public boolean unlock(long id) 
			throws MalformedURLException, SpringBatchClientException, IOException {

		Workflow workflow = getWorkflow(id);
		if (workflow == null) {
			return false;
		}

		synchronized (SynchronizationLockManager.getLockManager().getLock(Workflow.class, id)) {
			logger.debug("workflow " + id + " sync locked");

			// TODO here we may want to check whether the user is permitted to unlock the workflow
			workflow.removeLock();
			saveWorkflow(workflow);

			// successfully done
			logger.debug("workflow " + id + " sync unlocked");
			return true;
		}
	}

	/**
	 * locks the workflow with id for the active user
	 * 
	 * @param id
	 * @param sessionId
	 * @return
	 * @throws MalformedURLException
	 * @throws SpringBatchClientException
	 * @throws IOException
	 */
	public boolean lock(long id, String sessionId) 
			throws MalformedURLException, SpringBatchClientException, IOException {

		Workflow workflow = getWorkflow(id);
		if (workflow == null) {
			return false;
		}

		if (workflow.isLocked()) {
			return false; // is already locked
		}

		User user;
		Optional<User> oUser = userRepository.findBySession(sessionId);
		if (oUser.isPresent()) {
			user = oUser.get();
		} else {
			user = new User();
			user.setSession(sessionId);
			user.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
			userRepository.save(user);
			// TODO this should be an error once session management is implemented properly
		}

		UserLock userLock = new UserLock();
		userLock.setUser(user);
		userLock.setTime(System.currentTimeMillis());
		userLock.setSn(ApplicationSettings.getApplicationSettings().getNewSN());

		synchronized (SynchronizationLockManager.getLockManager().getLock(Workflow.class, id)) {
			logger.debug("workflow " + id + " sync locked");
			workflow.setLock(userLock);
			saveWorkflow(workflow);

			// successfully done
			logger.debug("workflow " + id + " sync unlocked");
			return true;
		}
	}
}