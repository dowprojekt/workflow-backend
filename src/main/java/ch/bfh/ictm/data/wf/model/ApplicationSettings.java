/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model;

import java.util.Optional;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import ch.bfh.ictm.data.wf.database.ApplicationSettingsRepository;
import ch.bfh.ictm.data.wf.rest.util.SpringApplicationContext;

/**
 * Manages the general / universal application settings.
 * Stores/retrieves a single application settings' object via JPA.
 * 
 * @author vgj1
 */

// Spring
@Component

// JPA
@Entity
@Access(value = AccessType.FIELD)
public class ApplicationSettings {
	@Transient
	private static final Logger logger = LogManager.getLogger(ApplicationSettings.class);

	@Transient
	private static int ID = 1;
	
	@Transient
	private static ApplicationSettingsRepository applicationSettingsRepository = null;
	
	@Transient
	private static ApplicationSettings applicationSettings = null;
	
	public static ApplicationSettings getApplicationSettings() {
		if (applicationSettings == null) {
			applicationSettingsRepository = SpringApplicationContext.getApplicationContext().getBean(ApplicationSettingsRepository.class);
			Optional<ApplicationSettings> applicationSettingsOptional = applicationSettingsRepository.findById(ID);
			if (applicationSettingsOptional.isPresent()) {
				applicationSettings = applicationSettingsOptional.get();
			} else {
				applicationSettings = SpringApplicationContext.getApplicationContext().getBean(ApplicationSettings.class);
			}
		}
		return applicationSettings;
	}

	@Id
	private final int id = ID; // there is only a single instance
	
	private final String appVersion = "0.12";
	
	private final String apiVersion = "0.13";
	
	private long sn; 
	
	protected ApplicationSettings() {
		logger.debug("application version " + appVersion + " / api version " + apiVersion);
	}
	
	private void save() {
		applicationSettingsRepository.save(this);
	}

	public String getAppVersion() {
		return appVersion;
	}

	public String getApiVersion() {
		return apiVersion;
	}
	
	public long getGlobalSN() {
		return sn;
	}
	
	public long getNewSN() {
		++sn;
		save();
		return sn;
		
	}
}
