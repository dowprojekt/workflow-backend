/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * represents a stand alone data function that can be called by the workflow. current
 * types are REST, WSDL, and APPLICATION. REST services are further detailed with the 
 * method (TODO may need to be extended for WSDL and application).
 * 
 * @author vgj1
 *
 */
@Entity
@Access(value=AccessType.FIELD)
public class DataFunction extends WorkflowObject {
	public enum Type {
		/**
		 * Undefined
		 */
		UNDEFINED(0),		
		
		/**
		 * WSDL Service call
		 */
		WSDL(1), 
		
		/**
		 * REST Service call
		 */
		REST(2), 
		
		/**
		 * local application call
		 */
		APPLICATION(3);
	
		private int id;
		
		private Type(int id) {
			this.id = id;
		}
		
		public static Type getType(int id) {
			for (Type e : Type.values()) {
				if (e.getId() == id) {
					return e;
				}
			}
			return Type.values()[0];
		}
		
		public int getId() {
			return id;
		}		
	}	
	
	public enum Method {
		
		/**
		 * not applicable for the selected service type
		 */
		NA(0), 
		
		/**
		 * GET method for REST service call
		 */
		GET(1), 
		
		/**
		 * POST method for REST service call
		 */
		POST(2), 
		
		/**
		 * PUT method for REST service call
		 */
		PUT(3), 
		
		/**
		 * DELETE method for REST service call
		 */
		DELETE(4);
		
		private int id;
		
		private Method(int id) {
			this.id = id;
		}
		
		public static Method getType(int id) {
			for (Method e : Method.values()) {
				if (e.getId() == id) {
					return e;
				}
			}
			return Method.values()[0];
		}
		
		public int getId() {
			return id;
		}			
	}
	
	private String name = "";
	
	@Lob // may be large
	private String description = "";
	
	@Lob // may be large
	private String uri = "";
	
	private int type = Type.UNDEFINED.getId();
	
	private int method; // for REST services
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<Parameter> parameters; // datafunction parameters valid for all workflows
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private UserLock userLock = null; // null means no lock
	
	public DataFunction() {
		method = Method.NA.getId();
		parameters = new ArrayList<Parameter>();
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Type getType() {
		return Type.getType(type);
	}
	
	public void setType(Type type) {
		this.type = type.getId();
	}
	
	public Iterator<Parameter> getParametersIterator() {
		return parameters.iterator();
	}
	
	public void addParameter(Parameter parameter) {
		if (parameter == null) return;
		parameters.add(parameter);
	}

	@Override
	public void update(ModelObject update) {
		super.update(update);
		this.description = ((DataFunction) update).description;
		this.method = ((DataFunction) update).method;
		this.name = ((DataFunction) update).name;
		this.type = ((DataFunction) update).type;
		this.uri = ((DataFunction) update).uri;
		
		this.parameters.clear();
		this.parameters.addAll(((DataFunction) update).parameters);
		resetParameters();
	}

	public Method getMethod() {
		return Method.getType(method);
	}

	public void setMethod(Method method) {
		this.method = method.getId();
	}
	
	public void resetParameters() {
		Iterator<Parameter> i = parameters.iterator();
		while (i.hasNext()) {
			Parameter parameter = i.next();
			parameter.resetId();
		}
	}
	
	public boolean isLocked() {
		return (userLock != null);
	}

	public void removeLock() {
		userLock = null;
	}
	
	public void setLock(UserLock userLock) {
		this.userLock = userLock;
	}
	
	public UserLock getLock() {
		return userLock;
	}
	
	/**
	 * checks whether this datafunction (on its own) is ready for execution
	 * 
	 * @return true if ready for execution, false otherwise
	 */
	public boolean isReady() {
		Iterator<Parameter> i = parameters.iterator();
		while (i.hasNext()) {
			if (!i.next().isReady()) {
				return false;
			}
		}

		if (uri.isEmpty()) {
			return false;
		}
		
		switch (getType()) {
		case APPLICATION:
			// TODO implement check
			break;
		case REST:
			if (!uri.startsWith("http")) {
				return false;
			}
			
			try {
				URI uriTest = new URI(uri);
			} catch (URISyntaxException e) {
				return false;
			}
			
			if (method == Method.NA.getId()) {
				return false;
			}
			break;
		case UNDEFINED:
			return false;
		case WSDL:
			// TODO implement check
			break;
		}
		
		return true; // everything ok
	}
	
	public String toString() {
		return "DF: id " + id 
				+ "; name " + name 
				+ " ; type " + type
				+ " ; method " + method 
				+ " ; uri " + uri 
				+ " ; #param "
				+ parameters.size() + " ; locked " + isLocked();
	}
}