/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ch.bfh.ictm.data.wf.model.ApplicationSettings;
import ch.bfh.ictm.data.wf.model.Tag;
import ch.bfh.ictm.data.wf.model.manager.TagManager;
import ch.bfh.ictm.data.wf.rest.model.TagResource;
import ch.bfh.ictm.data.wf.rest.util.HTTPResponse;

/**
 * Spring REST API configuration for /application - HATEOAS style
 * See the REST_API_Specification.txt for documentation.
 * 
 * @author vgj1
 *
 */
@RestController
@RequestMapping("/" + ApplicationSettingsRestController.PATH_APPLICATION_SETTINGS)
public class ApplicationSettingsRestController {
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public class TagNotFoundExeception extends Exception {
		private static final long serialVersionUID = 1L;

		public TagNotFoundExeception(String label) {
			super("could not find tag with label " + label);
		}
	}

	public static final String PATH_APPLICATION_SETTINGS = "application";
	public static final String PATH_TAGS = PATH_APPLICATION_SETTINGS + "/tags";
	public static final String PATH_VERSION = PATH_APPLICATION_SETTINGS + "/version";
	public static final String PATH_SN = PATH_APPLICATION_SETTINGS + "/seqno";
	
	private static final Logger logger = LogManager.getLogger(ApplicationSettingsRestController.class);
	
	public ApplicationSettingsRestController() {
	}

	@RequestMapping(value = "/version", method = RequestMethod.GET)
	@ResponseBody
	String readVersion(@CookieValue(value = "sessionid", defaultValue = "none") String sessionId) {
		logger.debug("received GET request to " + PATH_VERSION + " by user with id " + sessionId);
		return ApplicationSettings.getApplicationSettings().getApiVersion();
	}	

	@RequestMapping(value = "/seqno", method = RequestMethod.GET)
	@ResponseBody
	long readGlobalSN(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId) {
		logger.debug("received GET request to " + PATH_SN + " by user with id " + sessionId);
		return ApplicationSettings.getApplicationSettings().getGlobalSN();
	}	

	@RequestMapping(value = "/tags", method = RequestMethod.GET)
	Resources<TagResource> readTags(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId) {
		logger.debug("received GET request to " + PATH_TAGS + " by user with id " + sessionId);
		TagManager tagManager= TagManager.getManager();

		List<TagResource> tags = tagManager.getAllTags().stream().map(TagResource::new)
				.collect(Collectors.toList());

		logger.debug("tag list returned");
		return new Resources<TagResource>(tags);
	}	

	@RequestMapping(value = "/tags", method = RequestMethod.POST)
	ResponseEntity<?> createTag(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @RequestBody Tag in) {
		logger.debug("received POST request to " + PATH_TAGS + " by user with id " + sessionId);
		TagManager tagManager= TagManager.getManager();
		if (in.hasParent()) {
			tagManager.createTag(in.getLabel(), in.getParent().getLabel());
		} else {
			tagManager.createTag(in.getLabel());
		}

		logger.debug("tag created: " + in.getLabel());
		return HTTPResponse.createHTTPResponseCREATED(ServletUriComponentsBuilder.fromCurrentRequest().path("/{label}")
				.buildAndExpand(in.getLabel()).toUri());
	}

	@RequestMapping(value = "/tags/{label}", method = RequestMethod.PUT)
	ResponseEntity<?> updateTag(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable String label, @RequestBody Tag in) 
			throws TagNotFoundExeception, UnsupportedEncodingException {
		logger.debug("received PUT request to " + PATH_TAGS + "/" + label + " by user with id " + sessionId);
		TagManager tagManager= TagManager.getManager();
		
		Tag tag = tagManager.updateTag(URLDecoder.decode(label, "UTF-8"), in.hasParent() ? in.getParent().getLabel() : null);
		if (tag == null) {
			logger.error("tag with " + label + " does not exist");
			throw new TagNotFoundExeception(label);
		}
		
		logger.debug("tag updated: " + label);
		return HTTPResponse.createHTTPResponseOK(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
	}
	
	@RequestMapping(value = "/tags/{label}", method = RequestMethod.DELETE)
	ResponseEntity<?> deleteTag(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable String label) 
			throws TagNotFoundExeception, UnsupportedEncodingException {
		logger.debug("received DELETE request to " + PATH_TAGS + "/" + label + " by user with id " + sessionId);
		TagManager tagManager= TagManager.getManager();
		
		if (!tagManager.deleteTag(URLDecoder.decode(label, "UTF-8"))) {
			logger.error("tag with " + label + " does not exist");
			throw new TagNotFoundExeception(label);
		}

		logger.debug("tag deleted: " + label);
		return HTTPResponse.createHTTPResponseOK(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
	}
}
