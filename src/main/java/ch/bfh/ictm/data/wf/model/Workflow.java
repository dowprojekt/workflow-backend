/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import ch.bfh.ictm.data.wf.model.Execution.ExecutionResult;
import ch.bfh.ictm.data.wf.rest.springbatchclient.SpringBatchClient;
import ch.bfh.ictm.data.wf.rest.springbatchclient.SpringBatchClient.SpringBatchClientException;

/**
 * represents a workflow, consisting of (one or more) steps, consisting of one
 * ore more actions, containing one data function each. The workflow status
 * controls the modification and execution behavior of a workflow.
 * 
 * @author vgj1
 */
@Entity
@Access(value = AccessType.FIELD)
public class Workflow extends WorkflowObject {

	public enum Status {
		/**
		 * WF is being configured via the frontend
		 */
		CONFIGURATION(0), 	
		
		/**
		 * WF configuration is completed and WF ready to be deployed
		 */
		READY(1), 			
		
		/**
		 * WF is deployed on spring batch backend
		 */
		QUEUED(2), 			
		
		/**
		 * WF execution has been started
		 */
		RUNNING(3), 		
		
		/**
		 *  WF execution has been stopped manually
		 */
		STOPPED(4), 		
		
		/**
		 * WF execution has been successfully completed
		 */
		COMPLETED(5), 		
		
		/**
		 * failure during WF execution
		 */
		FAILED(6);			
		
		private int id;
		
		private Status(int id) {
			this.id = id;
		}
		
		public static Status getType(int id) {
			for (Status e : Status.values()) {
				if (e.getId() == id) {
					return e;
				}
			}
			return Status.values()[0];
		}
		
		public int getId() {
			return id;
		}
	}

	private String name = "";

	private String version = "";

	private int status = Status.CONFIGURATION.getId();

	private boolean published = false;

	@Lob // may be large
	private String description = "";
	
	@Lob // may be large
	private String targetGraph = "";
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private UserLock userLock = null; // null means no lock

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)	
	private List<Step> steps; // list of steps, each with one or more actions

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.EAGER)
	private ExecutionSettings executionSettings;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Execution> executionHistory; // the most current execution is the last element
	
	public Workflow() {
		executionSettings = new ExecutionSettings();
		executionHistory = new ArrayList<Execution>();
		steps = new ArrayList<Step>();
	}
	
	public Status getStatus() {
		return Status.getType(status);
	}

	public void setStatus(Status status) {
		this.status = status.getId();
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Iterator<Step> getStepsIterator() {
		return steps.iterator();
	}
	
	public void addStep(Step step) {
		steps.add(step);
	}
	
	public void addStep(int pos, Step step) {
		steps.add(pos, step);
	}
	
	public void updateStep(int pos, Step step) {
		steps.set(pos, step);
	}
	
	public Action getFirstAction() {
		if (steps.isEmpty()) return null;
		if (!steps.get(0).getActionsIterator().hasNext()) return null;
		return steps.get(0).getActionsIterator().next();
	}

	/**
	 * creates a serialized list of all actions in natural (i.e., insertion)
	 * order, in case a step contains more than 1 action
	 * 
	 * @return serialized list of all actions
	 */
	public ArrayList<Action> getSerialActions() {
		ArrayList<Action> actions = new ArrayList<Action>();
		
		for (Iterator<Step> i = steps.iterator(); i.hasNext(); ) {
			Step stepArrayList = i.next();
			for (Iterator<Action> j = stepArrayList.getActionsIterator(); j.hasNext(); ) {
				actions.add(j.next());
			}
		}

		return actions;
	}
	
	/**
	 * creates a serialized list of all data functions in natural (i.e., insertion)
	 * order, in case a step contains more than 1 action
	 * 
	 * @return serialized list of all data functions
	 */
	public ArrayList<DataFunction> getSerialDataFunctions() {
		ArrayList<DataFunction> dataFunctions = new ArrayList<DataFunction>();
		
		for (Iterator<Step> i = steps.iterator(); i.hasNext(); ) {
			Step stepArrayList = i.next();
			for (Iterator<Action> j = stepArrayList.getActionsIterator(); j.hasNext(); ) {
				dataFunctions.add(j.next().getDataFunction());
			}
		}

		return dataFunctions;
	}
	
	public int getNumberOfDataFunctions() {
		return getSerialActions().size();
	}
	
	public boolean hasDataFunction(long dataFunctionId) {
		Iterator<Step> i = steps.iterator();
		while (i.hasNext()) {
			Step step = i.next();
			if (step.hasDataFunction(dataFunctionId)) return true;
		}
		return false;
	}
	
	public void removeDataFunctionFromAllSteps(long dataFunctionId) {
		Iterator<Step> i = steps.iterator();
		while (i.hasNext()) {
			Step step = i.next();
			step.removeDataFunctionFromAllActions(dataFunctionId);
		}
	}

	public ExecutionSettings getExecutionSettings() {
		return executionSettings;
	}

	public void setExecutionSettings(ExecutionSettings executionSettings) {
		this.executionSettings = executionSettings;
	}

	public List<Execution> getExecutionHistory() {
		return executionHistory;
	}
	
	/**
	 * checks if the workflow is currently executed and returns the
	 * corresponding entry from the execution history.
	 * 
	 * @return the current/active execution, NULL if the workflow is not
	 *         running.
	 */
	public Execution getActiveExecution() {
		if (executionHistory.isEmpty()) {
			return null;
		}
		
		Execution latest = executionHistory.get(executionHistory.size() - 1);
		if (latest.getExecutionResult().equals(ExecutionResult.RUNNING)) {
			return latest; // we have an active execution
		}
		
		return null; // no active execution
	}
	
	public void addExecution(Execution execution) {
		executionHistory.add(execution);
	}
	
	@Override
	public void update(ModelObject update) {
		super.update(update);

		this.description = ((Workflow)update).description;
		this.name = ((Workflow)update).name;
		this.published = ((Workflow)update).published;
		//		this.status = ((Workflow)update).status; // status check is done elsewhere
		this.version = ((Workflow)update).version;

		//		this.executionHistory = ((Workflow)update).executionHistory; // TODO omit, right?
		this.executionSettings.update(((Workflow)update).executionSettings);

		this.targetGraph = ((Workflow)update).targetGraph;
		
		this.steps.clear();
		this.steps.addAll(((Workflow)update).steps); 

		replaceDataFunctions();
	}

	/**
	 * replaces all dependent objects referenced by a workflow object received
	 * from the WFE frontend with the persisted objects
	 */
	public void replaceDataFunctions() {
		Iterator<Step> i = steps.iterator();
		while (i.hasNext()) {
			Step step = i.next();
			step.resetId();
			Iterator<Action> j = step.getActionsIterator();
			while (j.hasNext()) {
				Action action = j.next();
				action.resetId();
				List<Tag> tags = action.getDataFunction().getTagsList();
				action.getDataFunction().replaceTags(tags);
				action.setDataFunction(action.getDataFunction(), true);
			}			
		}		
	}

	public void resetExecutionHistory() {
		executionHistory.clear();
	}
	
	public void initExecutionSettings() {
		executionSettings.resetId();
		if (executionSettings.hasSchedule()) {
			executionSettings.getSchedule().resetId();
		}
	}
	
	public String getTargetGraph() {
		return targetGraph;
	}

	public void setTargetGraph(String targetGraph) {
		this.targetGraph = targetGraph;
	}

	public boolean isLocked() {
		return (userLock != null);
	}

	public void removeLock() {
		userLock = null;
	}
	
	public void setLock(UserLock userLock) {
		this.userLock = userLock;
	}
	
	public UserLock getLock() {
		return userLock;
	}

	/**
	 * checks the current status of a running workflow and updates the status
	 * attribute accordingly. Note that an updated status is not persisted
	 * automatically but must be persisted by the calling method via the
	 * WorkflowManager if applicable. in order to support fast processing of
	 * many status request at the spring batch client, the spring batch session
	 * needs to be started and stopped by the calling function.
	 * 
	 * @return the updated status
	 * @throws IOException
	 * @throws SpringBatchClientException
	 * @throws MalformedURLException
	 */
	public Status checkStatus4Running() 
		throws MalformedURLException, SpringBatchClientException, IOException {

		if (getStatus() != Status.RUNNING) {
			return getStatus(); // this function is only for running workflows
		}
		
		// maybe it is FAILED or COMPLETED now
		SpringBatchClient sbc = SpringBatchClient.getSpringBatchClient();
		sbc.checkStatus(this); // updates the status if necessary
		return getStatus();
	}
	
	/**
	 * checks the current status of the workflow and updates the status
	 * attribute accordingly. Note that an updated status is not persisted
	 * automatically but must be persisted by the calling method via the
	 * WorkflowManager if applicable.
	 * 
	 * @return the updated status
	 * @throws IOException 
	 * @throws SpringBatchClientException 
	 * @throws MalformedURLException 
	 */
	public Status checkStatus() 
			throws MalformedURLException, SpringBatchClientException, IOException {
		// TODO implement additional checks
		
		switch (getStatus()) {
		case CONFIGURATION: {
			// maybe it is READY now
			Iterator<Step> i = steps.iterator();
			while (i.hasNext()) {
				if (!i.next().isReady()) {
					break; // not ready, status remains CONFIGURATION
				}
			}
			// found no problems so workflow status can be advanced to READY
			setStatus(Status.READY);
			break;
		}
		case READY:
		case QUEUED:
		case STOPPED:
		case FAILED:
		case COMPLETED: {
			// maybe it is not READY anymore
			Iterator<Step> i = steps.iterator();
			while (i.hasNext()) {
				if (!i.next().isReady()) {
					// found a problem
					setStatus(Status.CONFIGURATION);
					break; 
				}
			}
			// found no problems so workflow status remains the same
			break;
		}
		case RUNNING: {
			// maybe it is FAILED or COMPLETED now
			SpringBatchClient sbc = SpringBatchClient.getSpringBatchClient();
			sbc.startSession();
			sbc.checkStatus(this); // updates the status if necessary
			sbc.endSession();
			
			break;
		}
		}
		
		return getStatus();
	}
	
	public String toString() {
		String s = "WF: id " + id 
				+ "; name " + name 
				+ " ; status " + status
				+ " ; #steps " + steps.size() + " ; locked " + isLocked()
				+ " ; manual " + (!executionSettings.hasSchedule())
				+ " ; #history " + executionHistory.size();
		Iterator<DataFunction> i = getSerialDataFunctions().iterator();
		while (i.hasNext()) {
			s += "\n";
			s += i.next().toString();
		}
		return s;
	}
}