/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

/**
 * Represents a single execution of a workflow as part of the execution history
 * and as reported by the spring batch backend. An ongoing exection (=running 
 * workflow) has ExecutionResult NA.
 * 
 * @author vgj1
 */
@Entity
@Access(value=AccessType.FIELD)
public class Execution extends ModelObject {
	public enum ExecutionResult {
		
		/**
		 * currently running and no result available yet
		 */
		RUNNING (3), 
		
		/**
		 * execution stopped by user
		 */
		STOPPED (4),

		/**
		 * execution successful
		 */
		COMPLETED (5), 
		
		/**
		 * execution failed
		 */
		FAILED (6);
		
		
		private int id;
		
		private ExecutionResult(int id) {
			this.id = id;
		}
		
		public static ExecutionResult getType(int id) {
			for (ExecutionResult e : ExecutionResult.values()) {
				if (e.getId() == id) {
					return e;
				}
			}
			return ExecutionResult.values()[0];
		}
		
		public int getId() {
			return id;
		}
	}	
	
	private long executionTime; // ts in ms
	
	private long duration; // ts in ms
	
	private int executionResult;
	
	public Execution() {
	}
	
	public long getExecutiontime() {
		return executionTime;
	}
	
	public void setExecutiontime(long executiontime) {
		this.executionTime = executiontime;
	}
	
	public long getDuration() {
		return duration;
	}
	
	public void setDuration(long duration) {
		this.duration = duration;
	}
	
	public ExecutionResult getExecutionResult() {
		return ExecutionResult.getType(executionResult);
	}
	
	public void setExecutionResult(ExecutionResult executionResult) {
		this.executionResult = executionResult.getId();
	}
	
	@Override
	public void update(ModelObject update) {
		super.update(update);
		this.executionTime = ((Execution)update).executionTime;
		this.duration = ((Execution)update).duration;
		this.executionResult = ExecutionResult.RUNNING.getId(); // TODO reset ?
	}
}
