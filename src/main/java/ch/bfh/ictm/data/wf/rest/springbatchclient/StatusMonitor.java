/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.springbatchclient;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ch.bfh.ictm.data.wf.model.manager.WorkflowManager;
import ch.bfh.ictm.data.wf.rest.springbatchclient.SpringBatchClient.SpringBatchClientException;

/**
 * periodically monitors the status of all running workflows and notes status changes.
 * the monitor is started automatically on instantiation.
 * the interval in ms is retrieved from application.properties.
 * as single bean implements singleton pattern.
 * 
 * @author vgj1
 *
 */
@Component
public class StatusMonitor implements Runnable {
	private static final Logger logger = LogManager.getLogger(StatusMonitor.class);


	private long interval = 0;
	private Thread thread = null;
	
	@Autowired
	private StatusMonitor(@Value("${wfe.backend.monitoring-interval}") long interval) {
		this.interval = interval;		
	}
	

	public boolean isRunning() {
		return thread.isAlive();
	}

	/**
	 * @return the monitoring interval
	 */
	public long getInterval() {
		return interval;
	}

	@PostConstruct
	private void start() {
		logger.debug("starting up status monitor with interval " + interval);
		thread = new Thread(this);
		thread.start();
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 * 
	 * DO NOT CALL
	 * 
	 */
	@Override
	public void run() {
		while (true) {
			try {
				logger.debug("check status of all running workflows at spring batch backend...");
				WorkflowManager wfm = WorkflowManager.getManager();
				wfm.checkStatusFromAllRunningWorkflows();
				Thread.sleep(interval);
			} catch (SpringBatchClientException | IOException | InterruptedException e) {
				logger.error(e.getStackTrace());
			}
		}
	}
}
