/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import ch.bfh.ictm.data.wf.model.manager.DataFunctionManager;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * One Action of (one or more) in a workflow step. May have a condition (to
 * guard its execution), a data function and a list of parameters for that
 * data function.
 * 
 * @author vgj1
 *
 */
@Entity
@Access(value=AccessType.FIELD)
public class Action extends ModelObject {
	
	@JsonProperty("condition")
	@Lob // may be large
	private String _condition = ""; // use "_" to avoid sql naming issues (reserved names)
	
	@Lob // may be large
	private String description = "";

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER) 
	private DataFunction dataFunction;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<Parameter> parameters; // datafunction parameters specific to the workflow

	public Action() {
		parameters = new ArrayList<Parameter>();
	}

	public String getCondition() {
		return _condition;
	}

	public void setCondition(String condition) {
		this._condition = condition;
	}

	public DataFunction getDataFunction() {
		return dataFunction;
	}

	public void setDataFunction(DataFunction dataFunction) {
		this.dataFunction = dataFunction;
	}
	
	public void setDataFunction(DataFunction dataFunction, boolean lookupExisting) {
		if (lookupExisting) {
			this.dataFunction = DataFunctionManager.getManager().getDataFunction(dataFunction.id);
			
			if (this.dataFunction == null) {
				this.dataFunction = DataFunctionManager.getManager().createDataFunction(dataFunction);
			} else {
				this.dataFunction.update(dataFunction);				
			}
		} else {
			this.dataFunction.update(dataFunction);
		}
	}
	
	public Iterator<Parameter> getParametersIterator() {
		return parameters.iterator();
	}
	
	public void addParameter(Parameter parameter) {
		if (parameter == null) return;
		parameters.add(parameter);
	}

	@Override
	public void update(ModelObject update) {
		super.update(update);
		this._condition = ((Action) update)._condition;
		this.description = ((Action) update).description;
		
		this.dataFunction = DataFunctionManager.getManager().getDataFunction(update.id); 
		this.dataFunction.update(((Action) update).dataFunction);
		
		this.parameters.clear();
		this.parameters.addAll(((Action) update).parameters);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * checks whether this action (on its own) is ready for execution
	 * 
	 * @return true if ready for execution, false otherwise
	 */
	public boolean isReady() {
		if (!dataFunction.isReady()) {
			return false;
		}
		if (!_condition.isEmpty()) {
			// TODO check condition
		}
		Iterator<Parameter> i = parameters.iterator();
		while (i.hasNext()) {
			if (!i.next().isReady()) {
				return false;
			}
		}
		
		return true; // everything ok
	}
}
