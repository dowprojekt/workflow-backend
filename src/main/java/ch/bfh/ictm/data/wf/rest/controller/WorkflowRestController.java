/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import ch.bfh.ictm.data.wf.database.WorkflowRepository;
import ch.bfh.ictm.data.wf.model.Workflow;
import ch.bfh.ictm.data.wf.model.adapter.Adapter.ConvertWorkflowException;
import ch.bfh.ictm.data.wf.model.manager.WorkflowManager;
import ch.bfh.ictm.data.wf.rest.model.LockResource;
import ch.bfh.ictm.data.wf.rest.model.WorkflowResource;
import ch.bfh.ictm.data.wf.rest.springbatchclient.SpringBatchClient.SpringBatchClientException;
import ch.bfh.ictm.data.wf.rest.util.HTTPResponse;

/**
 * Spring REST API configuration for /workflows - HATEOAS style.
 * See the REST_API_Specification.txt for documentation.
 * 
 * @author vgj1
 * 
 */

@RestController
@RequestMapping("/" + WorkflowRestController.PATH_WORKFLOW)
public class WorkflowRestController {

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public class WorkflowException extends Exception {
		private static final long serialVersionUID = 1L;

		public WorkflowException(String message) {
			super(message);
			logger.error(getMessage());
		}
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	public class WorkflowNotFoundException extends Exception {
		private static final long serialVersionUID = 1L;

		public WorkflowNotFoundException(long workflowId) {
			super("could not find workflow with id " + workflowId);
			logger.error(getMessage());
		}
	}

	@ResponseStatus(HttpStatus.FORBIDDEN)
	public class WorkflowLockException extends Exception {
		private static final long serialVersionUID = 1L;

		public WorkflowLockException(long workflowId, String message) {
			super("workflow with id " + workflowId + " lock error: " + message);
			logger.error(getMessage());
		}
	}

	public static final String PATH_WORKFLOW = "workflows";
	
	private static final Logger logger = LogManager.getLogger(WorkflowRestController.class);

	private WorkflowRepository workflowRepository;

	@Autowired
	WorkflowRestController(WorkflowRepository workflowRepository) {
		this.workflowRepository = workflowRepository;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	Resources<WorkflowResource> readWorkflows(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId) 
			throws WorkflowException {
		logger.debug("received GET request to " + PATH_WORKFLOW + " by user with id " + sessionId);

		try {
			WorkflowManager.getManager().checkStatusFromAllWorkflows();
		} catch (SpringBatchClientException | IOException e) {
			throw new WorkflowException(e.getMessage());		
		}
		
		List<WorkflowResource> workflows = workflowRepository.findAll().stream().map(WorkflowResource::new)
				.collect(Collectors.toList());

		logger.debug("workflows returned");
		return new Resources<WorkflowResource>(workflows);
	}

	@RequestMapping(value = "/{workflowId}", method = RequestMethod.GET)
	WorkflowResource readWorkflowById(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long workflowId) 
			throws WorkflowNotFoundException, WorkflowException {
		logger.debug("received GET request to " + PATH_WORKFLOW + "/" + workflowId + " by user with id " + sessionId);		
		Workflow workflow = getWorkflow(workflowId);
		try {
			WorkflowManager.getManager().checkWorkflowStatus(workflowId);
		} catch (SpringBatchClientException | IOException e) {
			throw new WorkflowException(e.getMessage());		
		}
		logger.debug("workflow returned " + workflow);
		return new WorkflowResource(workflow);
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	ResponseEntity<?> createWorkflow(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @RequestBody Workflow workflow) 
			throws WorkflowException {
		logger.debug("received POST request to " + PATH_WORKFLOW + " by user with id " + sessionId);
		try {
			workflow = WorkflowManager.getManager().createWorkflow(workflow);
		} catch (SpringBatchClientException | IOException e) {
			throw new WorkflowException(e.getMessage());		
		}
		logger.debug("workflow created " + workflow);
		return HTTPResponse.createHTTPResponseCREATED(ServletUriComponentsBuilder.fromCurrentRequest().path("/{workflowId}")
				.buildAndExpand(workflow.getId()).toUri());
	}
	
	@RequestMapping(value = "/{workflowId}", method = RequestMethod.PUT)
	ResponseEntity<?> updateWorkflow(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long workflowId, @RequestBody Workflow workflowUpdate) 
			throws WorkflowNotFoundException, WorkflowLockException, WorkflowException {
		logger.debug("received PUT request to " + PATH_WORKFLOW + "/" + workflowId + " by user with id " + sessionId);
		
		Workflow workflow = getWorkflow(workflowId);
		
		if (workflow.isLocked()) {
			throw new WorkflowLockException(workflowId, "is currently locked");			
		}

		try {
			if (WorkflowManager.getManager().updateWorkflow(workflowId, workflowUpdate)) {
				logger.debug("workflow updated " + workflow);
				return HTTPResponse.createHTTPResponseOK(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
			} else {
				throw new WorkflowException("workflow " + workflowId + " could not be updated");
			}
		} catch (SpringBatchClientException | IOException| ConvertWorkflowException e) {
			throw new WorkflowException(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/{workflowId}", method = RequestMethod.DELETE)
	ResponseEntity<?> deleteWorkflow(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long workflowId) 
			throws WorkflowNotFoundException, WorkflowLockException, WorkflowException {
		logger.debug("received DELETE request to " + PATH_WORKFLOW + "/" + workflowId + " by user with id " + sessionId);

		Workflow workflow = getWorkflow(workflowId);
		
		if (workflow.isLocked()) {
			throw new WorkflowLockException(workflowId, "is currently locked");			
		}

		try {
			if (WorkflowManager.getManager().deleteWorkflow(workflowId)) {		
				logger.debug("workflow deleted");
				return HTTPResponse.createHTTPResponseOK(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
			} else {
				throw new WorkflowException("workflow " + workflowId + " delete failed");
			}
		} catch (SpringBatchClientException | IOException e) {
			throw new WorkflowException(e.getMessage());		
		}
	}
	
	@RequestMapping(value = "/{workflowId}/deploy", method = RequestMethod.PUT)
	ResponseEntity<?> deployWorkflow(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long workflowId) 
			throws WorkflowNotFoundException, WorkflowException {
		logger.debug("received PUT request to " + PATH_WORKFLOW + "/" + workflowId + "/deploy" + " by user with id " + sessionId);
		
		Workflow workflow = getWorkflow(workflowId);
		logger.debug("workflow: " + workflow);
		
		try {
			if (WorkflowManager.getManager().deployWorkflow(workflowId)) {		
				logger.debug("workflow deployed: " + workflow);
				return HTTPResponse.createHTTPResponseOK(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
			} else {
				throw new WorkflowException("workflow " + workflowId + " could not be deployed");
			}
		} catch (SpringBatchClientException | IOException| ConvertWorkflowException e) {
			throw new WorkflowException(e.getMessage());
		}
	}

	/**
	 * @param workflowId
	 * @throws WorkflowNotFoundException
	 */
	private Workflow getWorkflow(long workflowId)
			throws WorkflowNotFoundException {
		Optional<Workflow> workflowOptional = workflowRepository.findById(workflowId);
		if (!workflowOptional.isPresent()) {
			throw new WorkflowNotFoundException(workflowId);
		}
		return workflowOptional.get();
	}
	
	@RequestMapping(value = "/{workflowId}/executionstatus/{operation}", method = RequestMethod.PUT)
	ResponseEntity<?> changeWorkflowExecutionStatus(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long workflowId, @PathVariable String operation) 
			throws WorkflowNotFoundException, WorkflowException {
		logger.debug("received PUT request to " + PATH_WORKFLOW + "/" + workflowId + "/executionstatus/" + operation + " by user with id " + sessionId);
		
		Workflow workflow = getWorkflow(workflowId);
		logger.debug("workflow : " + workflow);
		
		try {
			if (WorkflowManager.getManager().changeWorkflowExecutionStatus(workflowId, operation)) {		
				logger.debug("workflow operation " + operation + " successful: " + workflow);
				return HTTPResponse.createHTTPResponseOK(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
			} else {
				throw new WorkflowException("workflow operation " + operation + " failed");
			}
		} catch (SpringBatchClientException | IOException | ConvertWorkflowException e) {
			throw new WorkflowException(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/{workflowId}/executionstatus", method = RequestMethod.GET)
	String readWorkflowExecutionStatus(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long workflowId) 
			throws WorkflowNotFoundException, WorkflowException {
		logger.debug("received GET request to " + PATH_WORKFLOW + "/" + workflowId + "/executionstatus" + " by user with id " + sessionId);
		Workflow workflow = getWorkflow(workflowId);
		logger.debug("old execution status: " + workflow.getStatus().name());
		try {
			if (WorkflowManager.getManager().checkWorkflowStatus(workflowId)) {		
				logger.debug("new execution status: " + workflow);
				return workflow.getStatus().name();
			} else {
				throw new WorkflowException("workflow check status failed");
			}
		} catch (SpringBatchClientException | IOException e) {
			throw new WorkflowException(e.getMessage());		
		}
	}

	@RequestMapping(value = "/{workflowId}/lock", method = RequestMethod.GET)
	ResponseEntity<?> readWorkflowLock(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long workflowId) 
			throws WorkflowNotFoundException {
		logger.debug("received GET request to " + PATH_WORKFLOW + "/" + workflowId + "/lock" + " by user with id " + sessionId);
		Workflow workflow = getWorkflow(workflowId);
		if (workflow.isLocked()) {
			logger.debug("current lock returned");
			return new ResponseEntity<LockResource>(new LockResource(workflow.getLock()), null, HttpStatus.OK);
		} else {
			logger.debug("workflow is not locked, empty response");
			return HTTPResponse.createHTTPResponseNOCONTENT(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
		}
	}
	
	@RequestMapping(value = "/{workflowId}/lock", method = RequestMethod.DELETE)
	ResponseEntity<?> deleteWorkflowLock(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long workflowId) 
			throws WorkflowNotFoundException, WorkflowLockException, WorkflowException {
		logger.debug("received DELETE request to " + PATH_WORKFLOW + "/" + workflowId + "lock" + " by user with id " + sessionId);

		getWorkflow(workflowId);

		try {
			if (WorkflowManager.getManager().unlock(workflowId)) {		
				logger.debug("workflow lock deleted");
				return HTTPResponse.createHTTPResponseOK(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
			} else {
				throw new WorkflowLockException(workflowId, "remove lock failed");		
			}
		} catch (SpringBatchClientException | IOException e) {
			throw new WorkflowException(e.getMessage());		
		}
	}	
	
	@RequestMapping(value = "/{workflowId}/lock", method = RequestMethod.PUT)
	ResponseEntity<?> lockWorkflow(@CookieValue(value = "sessionid", defaultValue= "none") String sessionId, @PathVariable long workflowId) 
			throws WorkflowNotFoundException, WorkflowLockException, WorkflowException {
		logger.debug("received PUT request to " + PATH_WORKFLOW + "/" + workflowId + "/lock" + " by user with id " + sessionId);

		getWorkflow(workflowId);

		try {
			if (WorkflowManager.getManager().lock(workflowId, sessionId)) {		
				logger.debug("workflow locked successfully");
				return HTTPResponse.createHTTPResponseOK(ServletUriComponentsBuilder.fromCurrentRequest().path("").build().toUri());
			} else {
				throw new WorkflowLockException(workflowId, "cannot be locked");		
			}
		} catch (SpringBatchClientException | IOException e) {
			throw new WorkflowException(e.getMessage());		
		}
	}
}