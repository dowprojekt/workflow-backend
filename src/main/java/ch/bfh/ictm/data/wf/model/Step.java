/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

/**
 * defines a certain step of a workflow, consisting of one or more actions (in
 * turn containing the data function to call). All actions are triggered
 * simulataneously for a step (parallel execution).
 * 
 * @author vgj1
 */
@Entity
@Access(value=AccessType.FIELD)
public class Step extends ModelObject {

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Action> actions;
	
	public Step() {
		actions = new ArrayList<Action>();
	}
	
	public Iterator<Action> getActionsIterator() {
		return  actions.iterator();
	}
	
	public void addAction(Action action) {
		actions.add(action);
	}
	
	public void addAction(int pos, Action action) {
		actions.add(pos, action);
	}
	
	public boolean hasDataFunction(long id) {
		Iterator<Action> i = actions.iterator();
		while (i.hasNext()) {
			Action action = i.next();
			if (action.getDataFunction().equals(id)) return true;
		}
		return false;
	}
	
	public void removeDataFunctionFromAllActions(long id) {
		while (hasDataFunction(id)) {
			Iterator<Action> i = actions.iterator();
			while (i.hasNext()) {
				Action action = i.next();
				if (action.getDataFunction().equals(id)) {
					actions.remove(action);
					break;
				}
			}
		}
	}
	
	@Override
	public void update(ModelObject update) {
		super.update(update);
		this.actions.clear();
		this.actions.addAll(((Step)update).actions);
	}
	
	/**
	 * checks whether this step (on its own) is ready for execution
	 * 
	 * @return true if ready for execution, false otherwise
	 */
	public boolean isReady() {
		Iterator<Action> i = actions.iterator();
		while (i.hasNext()) {
			Action action = i.next();
			if (!action.isReady()) {
				return false; // found an action that is not ready
			}
		}
		return true; // everything ok
	}
}
