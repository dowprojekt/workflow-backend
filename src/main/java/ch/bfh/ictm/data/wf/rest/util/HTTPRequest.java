/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author vgj1
 *
 */
public class HTTPRequest {
	
	public static final String charset = java.nio.charset.StandardCharsets.UTF_8.name();
	
	private static final Logger logger = LogManager.getLogger(HTTPRequest.class);
	
	/**
	 * send HTTP GET Request 
	 * 
	 * @param requestUrl
	 * @return
	 * @throws IOException
	 */
	public static HttpURLConnection sendGET(String requestUrl) throws IOException {
		return sendRequest(requestUrl, "GET", null, null);
	}
	
	/**
	 * send HTTP GET Request 
	 * 
	 * @param requestUrl
	 * @param cookies set NULL if not required
	 * @return
	 * @throws IOException
	 */
	public static HttpURLConnection sendGET(String requestUrl, List<String> cookies) throws IOException {
		return sendRequest(requestUrl, "GET", cookies, null);
	}
	
	/**
	 * send HTTP POST Request with data encoded as Json
	 * 
	 * @param requestUrl
	 * @param cookies set NULL if not required
	 * @param data
	 * @return
	 * @throws IOException
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 */
	public static HttpURLConnection sendPOST(String requestUrl, List<String> cookies, Object data) 
			throws IOException, JsonGenerationException, JsonMappingException {

		return sendRequest(requestUrl, "POST", cookies, data);
	}

	/**
	 * use jackson json mapper to serialize data into the HTTP request body
	 * 
	 * @param data
	 * @param requestConnection
	 * @throws IOException
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 */
	private static void serializeContent(Object data, HttpURLConnection requestConnection) 
			throws IOException,	JsonGenerationException, JsonMappingException {
		
		OutputStream os = requestConnection.getOutputStream();

		// configuration of Jackson POJO->Json mapper
		// valid for everything we serialize, namely ch.bfh.ictm.data.wf.model.* and com.ontos.ldiw.workflow.beans.*
		ObjectMapper om = new ObjectMapper();		
		om.setVisibility(PropertyAccessor.FIELD, Visibility.ANY); 
        om.setVisibility(PropertyAccessor.GETTER, Visibility.NONE); 
        om.setVisibility(PropertyAccessor.IS_GETTER, Visibility.NONE);
        om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		om.writeValue(os, data);
		
		logger.debug("request body: " + om.writeValueAsString(data));
	}
	
	/**
	 * send HTTP POST with data encoded as key/value pairs and cookies.
	 * 
	 * @param requestUrl
	 * @param cookies set NULL if not required
	 * @param content set NULL if not required
	 * @return the HttpURLConnection
	 * @throws IOException
	 */
	public static HttpURLConnection sendPOST(String requestUrl, List<String> cookies, Map<String, String> content) 
			throws IOException {
		
		String body = (content == null) ? null : serializeContent(content);
		return sendRequest(requestUrl, "POST", cookies, body, "application/x-www-form-urlencoded");
	}
		
	/**
	 * send HTTP PUT with data encoded as key/value pairs and cookies.
	 * 
	 * @param requestUrl
	 * @param cookies set NULL if not required
	 * @param content set NULL if not required
	 * @return the HttpURLConnection
	 * @throws IOException
	 */
	public static HttpURLConnection sendPUT(String requestUrl, List<String> cookies, Map<String, String> content) 
			throws IOException {
		
		String body = (content == null) ? null : serializeContent(content);
		return sendRequest(requestUrl, "PUT", cookies, body, "application/x-www-form-urlencoded");
	}

	/**
	 * send HTTP DELETE with cookies.
	 * 
	 * @param requestUrl
	 * @param cookies set NULL if not required
	 * @return the HttpURLConnection
	 * @throws IOException
	 */
	public static HttpURLConnection sendDELETE(String requestUrl, List<String> cookies) 
			throws IOException {
		return sendRequest(requestUrl, "DELETE", cookies, null);
	}
		
	/**
	 * encode content parameters as key-value pairs for request body
	 * @param content
	 * @return the serialized content
	 * @throws UnsupportedEncodingException
	 */
	private static String serializeContent(Map<String, String> content)
			throws UnsupportedEncodingException {
		StringBuffer body = new StringBuffer();
		Iterator<String> keyIterator = content.keySet().iterator();
		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			String value = content.get(key);
			body.append(key + "=" + URLEncoder.encode(value, charset));
			if (keyIterator.hasNext()) {
				body.append("&"); // add separator
			}
		}
		return body.toString();
	}

	/**
	 * send HTTP request with data encoded as key/value pairs and cookies.
	 * 
	 * @param requestUrl
	 * @param method one of: POST, PUT, DELETE, GET
	 * @param cookies set NULL if not required
	 * @param content set NULL if not required
	 * @param contentType define content type (e.g., "application/json" or "application/x-www-form-urlencoded"), else set NULL if no content is present
	 * @return the HttpURLConnection
	 * @throws IOException
	 */
	private static HttpURLConnection sendRequest(String requestUrl, String method, List<String> cookies, String content, String contentType) 
			throws IOException {
		
		logger.debug("HTTPRequest.sendRequest(): method " + method + " to " + requestUrl);
		HttpURLConnection requestConnection = (HttpURLConnection) new URL(requestUrl).openConnection();
		if (content != null) {
			requestConnection.setDoOutput(true);
		}
		requestConnection.setInstanceFollowRedirects(false);
		requestConnection.setRequestProperty("Accept-Charset", charset);
		requestConnection.setRequestMethod(method);
		if (contentType != null) {
			requestConnection.setRequestProperty("Content-Type", contentType + ";charset=" + charset);
		}
		
		// add all cookies to request header
		if (cookies != null) {
			addCookiesToHeader(cookies, requestConnection);
		}		
		
		if (content != null) {
			OutputStream os = requestConnection.getOutputStream();
			os.write(content.toString().getBytes(charset));
			logger.debug("request body: " + content);
		}
		
		// send request
		requestConnection.connect();
		return requestConnection;
	}
	
	/**
	 * send HTTP request with POJO data to be encoded as Json
	 * 
	 * @param requestUrl
	 * @param method one of: POST, PUT, DELETE, GET
	 * @param cookies set null if not required
	 * @param content the POJO content to be included in the request body, set null if not required
	 * @return if successful, the HttpURLConnection
	 * @throws IOException
	 */
	private static HttpURLConnection sendRequest(String requestUrl, String method, List<String> cookies, Object content) 
			throws IOException {
		
		logger.debug("HTTPRequest.sendRequest(): method " + method + " to " + requestUrl);
		HttpURLConnection requestConnection = (HttpURLConnection) new URL(requestUrl).openConnection();
		if (content != null) {
			requestConnection.setDoOutput(true);
		}
		requestConnection.setInstanceFollowRedirects(false);
		requestConnection.setRequestMethod(method);
		if (content != null) {
			requestConnection.setRequestProperty("Content-Type", "application/json");
		}
		
		// add all cookies to request header
		if (cookies != null) {
			addCookiesToHeader(cookies, requestConnection);
		}		
		
		// serialize the content POJO as Json
		if (content != null) {
			serializeContent(content, requestConnection);
		}
		
		// send request
		requestConnection.connect();
		return requestConnection;
	}

	/**
	 * write the given list of cookies (each in the form of "cookie=value") to the request header
	 * 
	 * @param cookies
	 * @param requestConnection
	 */
	private static void addCookiesToHeader(List<String> cookies, HttpURLConnection requestConnection) {
		StringBuffer cookie = new StringBuffer();
		Iterator<String> cookieIterator = cookies.iterator();
		while (cookieIterator.hasNext()) {
			cookie.append(cookieIterator.next());
			if (cookieIterator.hasNext()) {
				cookie.append("; ");
			}
		}
		requestConnection.setRequestProperty("Cookie", cookie.toString());
		logger.debug("set Cookie: " + cookie);
	}

	/**
	 * send HTTP PUT Request with data encoded as Json
	 * 
	 * @param requestUrl
	 * @param cookies set null if not required
	 * @param data
	 * @return
	 * @throws IOException
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 */
	public static HttpURLConnection sendPUT(String requestUrl, List<String> cookies, Object data) 
			throws IOException, ProtocolException, JsonGenerationException, JsonMappingException {

		return sendRequest(requestUrl, "PUT", cookies, data);
	}
	
	/**
	 * retrieve HTTP Response Body as String
	 * 
	 * @param connection
	 * @return
	 * @throws IOException
	 */
	public static String readResponse(HttpURLConnection connection) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

		StringBuffer result = new StringBuffer();
		String line = reader.readLine();
		while (line != null) {
			result.append(line);
			line = reader.readLine();
		}
		logger.debug("HTTPRequest.readResponse - read " + result.length() + " characters");
		
		return result.toString();
	}
	
	/**
	 * send HTTP DELETE request
	 * 
	 * @param requestUrl
	 * @return
	 * @throws IOException
	 * @throws ProtocolException
	 */
	public static HttpURLConnection sendDELETE(String requestUrl) throws IOException {
		return sendRequest(requestUrl, "DELETE", null, null);
	}
}
