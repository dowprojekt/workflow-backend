/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.model;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import org.springframework.hateoas.ResourceSupport;

import ch.bfh.ictm.data.wf.model.DataFunction;
import ch.bfh.ictm.data.wf.rest.controller.DataFunctionRestController;

/**
 * @author vgj1
 *
 */
public class DataFunctionResource extends ResourceSupport {
	private final DataFunction dataFunction;
	
	public DataFunctionResource(DataFunction dataFunction) {
		this.dataFunction = dataFunction;
		this.add(linkTo(DataFunctionRestController.class, dataFunction.getId()).withRel(DataFunctionRestController.PATH_DATAFUNCTION));
	}

	public DataFunction getDataFunction() {
		return dataFunction;
	}
}
		