/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Filter for allowing requests from any source (CORS)
 *
 * https://spring.io/guides/gs/rest-service-cors/
 */
@Component
public class SimpleCORSFilter implements Filter {
	
	@Value("${wfe.frontend.cors-enabled}")
	private boolean cors_is_enabled;
	@Value("${wfe.frontend.client-url}")
	private String cors_client_url;
	
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {		
		if (cors_is_enabled) {

			// see http://www.w3.org/TR/cors/
			// and (more human-friendly): http://www.html5rocks.com/en/tutorials/cors/
			// http://www.html5rocks.com/static/images/cors_server_flowchart.png
			HttpServletResponse response = (HttpServletResponse) res;
			response.setHeader("Access-Control-Allow-Origin", cors_client_url); // cannot set to * if Access-Control-Allow-Credentials is true ;
			response.setHeader("Access-Control-Allow-Credentials", "true"); // required if we want to include Cookie information with the CORS
			response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
			response.setHeader("Access-Control-Max-Age", "3600");
			// "The Access-Control-Allow-Headers header indicates, as part of the response to a preflight request, which header field names can be used during the actual request."
			response.setHeader("Access-Control-Allow-Headers", "Content-Type");  
			// "The Access-Control-Expose-Headers header indicates which headers are safe to expose to the API of a CORS API specification."
			// also need to EXPOSE the Location header, which will be used to retrieve the id of the new objects being created
			response.setHeader("Access-Control-Expose-Headers", "Location");
			chain.doFilter(req, res);
		}
	}

	public void init(FilterConfig filterConfig) {}

	public void destroy() {}

}
