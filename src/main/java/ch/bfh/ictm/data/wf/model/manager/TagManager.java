/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model.manager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ch.bfh.ictm.data.wf.database.TagRepository;
import ch.bfh.ictm.data.wf.model.ApplicationSettings;
import ch.bfh.ictm.data.wf.model.Tag;
import ch.bfh.ictm.data.wf.model.manager.util.SynchronizationLockManager;
import ch.bfh.ictm.data.wf.rest.util.SpringApplicationContext;

/**
 * manage the creation, modification, deletion, and persistence of Tag
 * objects manipulated via the WFE frontend.
 * 
 * is implemented as singleton that is retrieved via the static method getManager().
 * 
 * @author vgj1
 *
 */

//Spring
@Component
// Spring JPA
@Transactional
public class TagManager {
	public static TagManager getManager() {
		return SpringApplicationContext.getApplicationContext().getBean(TagManager.class);
	}

	@Autowired
	private TagRepository tagRepository;
	
	protected TagManager() {
	}
	
	public Iterator<Tag> getTagsIterator() {
		return tagRepository.findAll().iterator();
	}
	
	public List<Tag> getAllTags() {
		return tagRepository.findAll();
	}
	
	/**
	 * retrieves tag by its label
	 * 
	 * @param label
	 * @return tag if exists, else null
	 */
	public Tag getTag(String label) {
		if ((label == null) || (label.isEmpty())) return null;
		Optional<Tag> oTag = tagRepository.findByLabel(label);
		return (oTag.isPresent() ? oTag.get() : null);
	}
	
	/**
	 * creates a new tag with the label given. Existing tags will be ignored,
	 * i.e., there can be only one tag with the same label. All changes are
	 * saved to the DB.
	 * 
	 * @param label
	 * @return the added Tag
	 */
	public Tag createTag(String label) {
		if (label == null) return null;
		return createTag(label, null);
	}
	
	/**
	 * creates a new tag with the label given and the parent tag identified via
	 * the parentLabel given. If there is no parent tag with parentLabel yet, it
	 * is created too. Existing tags will be ignored, i.e., there can be only
	 * one tag with the same label. All changes are saved to the DB.
	 * 
	 * @param label
	 * @param parentLabel
	 * @return the added Tag
	 */
	public synchronized Tag createTag(String label, String parentLabel) {
		if (label == null) return null;
		
		Tag tag = getTag(label);
		if (tag == null) {
			tag = new Tag(label);
			tag.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
			tagRepository.save(tag);
		}
		if (parentLabel != null) {
			Tag parentTag = getTag(parentLabel);
			if (parentTag == null) {
				parentTag = new Tag(parentLabel);
				parentTag.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
				tagRepository.save(parentTag);
			}
			tag.setParent(parentTag);
			tag.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
		}
		return tag;
	}
	
	/**
	 * changes an existing tag with the label given and the parent tag identified via
	 * the parentLabel given. If there is no parent tag with parentLabel yet, it
	 * is created. Existing tags will be ignored, i.e., there can be only
	 * one tag with the same label. All changes are saved to the DB.
	 * 
	 * @param label
	 * @param parentLabel
	 * @return the added Tag
	 */
	public Tag updateTag(String label, String parentLabel) {
		if (label == null) return null;

		synchronized (SynchronizationLockManager.getLockManager().getLock(Tag.class, label)) {
			Tag tag = getTag(label);
			if (tag == null)
				return null;

			tag.setLabel(label);

			if (parentLabel != null) {
				Tag parent = getTag(parentLabel);
				if (parent == null) {
					parent = createTag(parentLabel);
				}
				tag.setParent(parent);
			}

			tag.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
			tagRepository.save(tag);
			return tag;
		}
	}
	
	/**
	 * delete tag by its label
	 * 
	 * @param label
	 * @return true if successful, else false (tag not found)
	 */
	public boolean deleteTag(String label) {
		if (label == null) return false;

		synchronized (SynchronizationLockManager.getLockManager().getLock(Tag.class, label)) {
			Tag tag = getTag(label);
			if (tag == null)
				return false;

			tag.setSn(ApplicationSettings.getApplicationSettings().getNewSN());
			tagRepository.delete(tag);

			return true;
		}
	}
	
	/**
	 * retrieves all corresponding tags from the DB (with proper IDs). Tags that
	 * do not exist yet are created automatically.
	 * 
	 * @param inTagList
	 * @return the list of persisted tags
	 */
	public List<Tag> getTags(List<Tag> inTagList) {
		List<Tag> tagList = new ArrayList<Tag>();
		
		Iterator<Tag> i = inTagList.iterator();
		while (i.hasNext()) {
			Tag tmp = i.next();
			tagList.add(createTag(tmp.getLabel(), (tmp.hasParent()) ? tmp.getParent().getLabel() : null));
		}
		
		return tagList;
	}
}
