/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model.adapter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import ch.bfh.ictm.data.wf.rest.util.HTTPRequest;

/**
 * serializes the WFE workflow data model to the workflow data model used by the
 * springbatch client (in turn defined in com.ontos.ldiw.workflow.beans). make
 * sure to update these when the springbatch client model changes.
 * 
 * @author vgj1
 *
 */
public class Adapter {
	public static class ConvertWorkflowException extends Exception {
		private static final long serialVersionUID = 1L;

		public ConvertWorkflowException(String e) {
			super(e);
		}		
	}
		
	private static final String WORKFLOW_PREFIX = "wf_" + System.currentTimeMillis() + "_"; 
	
	private static final Logger logger = LogManager.getLogger(Adapter.class);
	
	/**
	 * unique name of workflow required by the spring batch backend
	 * 
	 * @param workflowId
	 * @return
	 */
	public static String getWorkflowName(long workflowId) {
		return WORKFLOW_PREFIX + workflowId;
	}
	
	/**
	 * parse workflow id from spring batch backend name
	 * 
	 * @param workflowName
	 * @return
	 */
	public static long getWorkflowId(String workflowName) {
		return new Long(workflowName.substring(WORKFLOW_PREFIX.length()+1)).longValue();
	}

	/**
	 * creates a spring batch backend workflow (i.e., MultiStepJob) from the given workflow.
	 * 
	 * @param workflow
	 * @return
	 * @throws ConvertWorkflowException for conversion errors
	 */
	public static com.ontos.ldiw.workflow.beans.MultiStepJob convert(ch.bfh.ictm.data.wf.model.Workflow workflow) throws ConvertWorkflowException {
		// basic configuration
		com.ontos.ldiw.workflow.beans.MultiStepJob msj = new com.ontos.ldiw.workflow.beans.MultiStepJob();
		msj.setName(getWorkflowName(workflow.getId())); // name needs to be unique so we use the id
		msj.setLabel(workflow.getName());
		msj.setDescription(workflow.getDescription());
		msj.setTargetGraph(workflow.getTargetGraph());
		
		// schedule
		if (workflow.getExecutionSettings().hasSchedule()) {
			com.ontos.ldiw.workflow.beans.Schedule schedule = new com.ontos.ldiw.workflow.beans.Schedule();
			schedule.setStart(workflow.getExecutionSettings().getSchedule().getStart());
			schedule.setEnd(workflow.getExecutionSettings().getSchedule().getEnd());
			schedule.setIntervalDay(workflow.getExecutionSettings().getSchedule().isIntervalDay());
			schedule.setIntervalWeek(workflow.getExecutionSettings().getSchedule().isIntervalWeek());
			schedule.setIntervalMonth(workflow.getExecutionSettings().getSchedule().isIntervalMonth());
			msj.setSchedule(schedule);
		} // else null value
		
		// steps
		List<com.ontos.ldiw.workflow.beans.Step> steps = new ArrayList<com.ontos.ldiw.workflow.beans.Step>();
		Iterator<ch.bfh.ictm.data.wf.model.Action> i = workflow.getSerialActions().iterator(); // reminder: this is a flattened list of workflow steps
		int counter = 1;
		while (i.hasNext()) {
			ch.bfh.ictm.data.wf.model.Action action = i.next();

			// TODO this is currently only implemented for REST services
			if (!action.getDataFunction().getType().equals(ch.bfh.ictm.data.wf.model.DataFunction.Type.REST)) {
				String message = "currently only REST services are supported - fatal error";
				logger.error(message);
				throw new ConvertWorkflowException(message);
			}
			
			com.ontos.ldiw.workflow.beans.Step step = new com.ontos.ldiw.workflow.beans.Step();
			step.setService(action.getDataFunction().getUri());
			step.setMethod(action.getDataFunction().getMethod().toString());
			
			// parameter - for REST services there is max. 1 Parameter with name of "body"
			if (action.getDataFunction().getParametersIterator().hasNext()) {
				ch.bfh.ictm.data.wf.model.Parameter parameter = action.getDataFunction().getParametersIterator().next();
				step.setContenttype(parameter.getType());
				try {
					step.setBody(URLEncoder.encode(parameter.getValue(), HTTPRequest.charset));
				} catch (UnsupportedEncodingException e) {
					String message = "URL encoding the service body failed - fatal error";
					logger.error(message);
					throw new ConvertWorkflowException(message);
				}
			} else {
				step.setContenttype("");
				step.setBody("");
			}
			
			step.setNumberOfOrder(counter++);
			steps.add(step);
		}		
		
		msj.setSteps(steps);
		
		// done
		logger.debug("created MultiStepJop: name = " + msj.getName() + " ; label = " + msj.getLabel() + " ; nbr of steps = " + msj.getSteps().size());
		
		return msj;
	}
}
