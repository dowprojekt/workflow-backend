/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.util;

import java.io.IOException;
import java.net.URI;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author vgj1
 *
 */
public class HTTPResponse {
	public static ResponseEntity<?> createHTTPResponseOK(URI location) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(location);
		return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
	}
	
	public static ResponseEntity<?> createHTTPResponseNOCONTENT(URI location) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(location);
		return new ResponseEntity<>(null, httpHeaders, HttpStatus.NO_CONTENT);
	}
	
	public static ResponseEntity<?> createHTTPResponseCREATED(URI location) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(location);
		return new ResponseEntity<>(null, httpHeaders, HttpStatus.CREATED);
	}
	
	public static <T> T json2Java(final TypeReference<T> type, final String json) 
			throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(json, type);
	}
}
