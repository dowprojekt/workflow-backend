/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest.springbatchclient;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ch.bfh.ictm.data.wf.model.Workflow;
import ch.bfh.ictm.data.wf.model.Workflow.Status;
import ch.bfh.ictm.data.wf.model.adapter.Adapter;
import ch.bfh.ictm.data.wf.model.adapter.Adapter.ConvertWorkflowException;
import ch.bfh.ictm.data.wf.rest.util.HTTPRequest;
import ch.bfh.ictm.data.wf.rest.util.SpringApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ontos.ldiw.workflow.beans.MultiStepJob;

/**
 * communicates with the spring batch client via REST. controls the spring batch
 * client status monitor.
 * 
 * implements singleton pattern.
 * 
 * @author vgj1
 *
 */
@Component
public class SpringBatchClient {
	
	public class SpringBatchClientException extends Exception {
		private static final long serialVersionUID = 1L;

		public SpringBatchClientException(String e) {
			super(e);
		}
	}
	
	private static final Logger logger = LogManager.getLogger(SpringBatchClient.class);

	private static SpringBatchClient springBatchClient = null;
	
	public static SpringBatchClient getSpringBatchClient() {
		if (springBatchClient == null) {
			springBatchClient = SpringApplicationContext.getApplicationContext().getBean(SpringBatchClient.class);
		}
		return springBatchClient;
	}
	
	private String SBS_URL;
	private String SBS_AUTHENTICATE_URL;
	private String SBS_SESSION_URL;
	private String SBS_JOBS_URL;	
	private static final String SBS_STATUS_TAG = "\"status\"";
	
	private String sbsUsername; 	
	private String sbsPassword; 
	private String sbsSessionToken;
	private String sbsSessionUser;
	private String sbsSessionURL;
	
	@Autowired
	private SpringBatchClient(@Value("${sbs.url}") String sbs_url, @Value("${sbs.user}") String sbs_user, @Value("${sbs.password}") String sbs_password) {
		SBS_URL = sbs_url;
		SBS_AUTHENTICATE_URL = SBS_URL + "/AuthenticationServlet";
		SBS_SESSION_URL = SBS_URL + "/rest/session";
		SBS_JOBS_URL = SBS_URL + "/rest/jobs";
		
		logger.debug("spring batch server URL: " + SBS_URL);
		
		sbsUsername = sbs_user;
		sbsPassword = sbs_password; 
		sbsSessionToken = null;
		sbsSessionUser = null;
		sbsSessionURL = null;	
	}
	
	/**
	 * starts a new session with the spring batch backend: authenticates the
	 * user as configured and start the session
	 * 
	 * the session needs to be closed explicitly by calling endSession().
	 * 
	 * @return true on success, else throws one of the exceptions below
	 * @throws SpringBatchClientException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public boolean startSession() 
			throws SpringBatchClientException, MalformedURLException, IOException {
		
		logger.debug("starting spring batch backend session...");
		
		clearSession();
		
		// user login
		HashMap<String, String> loginParameters = new HashMap<String, String>();
		loginParameters.put("mode", "login");
		loginParameters.put("username", sbsUsername);
		loginParameters.put("password", sbsPassword);
		HttpURLConnection requestConnection = HTTPRequest.sendPOST(SBS_AUTHENTICATE_URL, null, loginParameters);
		if (requestConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
			String message = "login to spring batch server at "
					+ SBS_AUTHENTICATE_URL
					+ " with given username/password failed, response code is "
					+ requestConnection.getResponseCode();
			logger.error(message);
			throw new SpringBatchClientException(message);
		}		
		
		// read cookies
		sbsSessionToken = "";
		sbsSessionUser = "";
		Iterator<String> cookiesIterator = requestConnection.getHeaderFields().get("Set-Cookie").iterator();
		while (cookiesIterator.hasNext()) {
			String cookie = cookiesIterator.next();
			if (cookie.startsWith("token")) {
				sbsSessionToken = cookie.substring("token=".length()); // remove "token="
			} else if (cookie.startsWith("user")) {
				sbsSessionUser = cookie.substring("user=".length()); // remove "user="
			}
		}
		if (sbsSessionToken.isEmpty() || sbsSessionUser.isEmpty()) {
			clearSession();
			String message = "login to spring batch server at " + SBS_AUTHENTICATE_URL
					+ "failed, no cookies for token/user returned";
			logger.error(message);
			throw new SpringBatchClientException(message);
		}

		// session start
		requestConnection = HTTPRequest.sendPUT(SBS_SESSION_URL, getCookieList(), null);
		if (requestConnection.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
			clearSession();
			String message = "creating session at spring batch server at "
					+ SBS_SESSION_URL
					+ " failed, response code is "
					+ requestConnection.getResponseCode();
			logger.error(message);
			throw new SpringBatchClientException(message);
		}
		String responseBody = HTTPRequest.readResponse(requestConnection);
		if (responseBody.isEmpty() || (responseBody.indexOf("endpoint") == -1)) {
			clearSession();
			String message = "login to spring batch server at " + SBS_SESSION_URL
					+ "failed, no value for endpoint returned";
			logger.error(message);
			throw new SpringBatchClientException(message);			
		}
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonData = mapper.readValue(responseBody, Map.class);
		sbsSessionURL = SBS_URL + "/" + (String) jsonData.get("endpoint");
		checkSession();
		
		// successfully done
		logger.debug("spring batch backend session started");
		return true;
	}

	private List<String> getCookieList() {
		if ((sbsSessionUser == null) || sbsSessionToken == null) return null;
		List<String> cookies = new ArrayList<String>();
		cookies.add("token=" + sbsSessionToken);
		cookies.add("user=" + sbsSessionUser);
		return cookies;
	}
	
	private void clearSession() {
		logger.debug("clear session parameters");
		sbsSessionToken = null;
		sbsSessionUser = null;
		sbsSessionURL = null;
	}

	/**
	 * ends the current session with the spring batch client
	 * 
	 * @return false if no session is active, true on success, on error throws
	 *         one of the exceptions below.
	 * @throws SpringBatchClientException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public boolean endSession() 
			throws SpringBatchClientException, MalformedURLException, IOException {

		logger.debug("ending spring batch backend session...");

		if (sbsSessionToken == null) {
			logger.error("no active spring batch backend session");
			return false;
		}
		
		// session end
		HttpURLConnection requestConnection = HTTPRequest.sendDELETE(sbsSessionURL, getCookieList());
		if (requestConnection.getResponseCode() != HttpURLConnection.HTTP_NO_CONTENT) {
			String message = "deleting session at spring batch server at "
					+ sbsSessionURL
					+ " failed, response code is "
					+ requestConnection.getResponseCode();
			clearSession();
			logger.error(message);
			throw new SpringBatchClientException(message);
		}

		// user logout
		HashMap<String, String> logoutParameters = new HashMap<String, String>();
		logoutParameters.put("mode", "logout");
		logoutParameters.put("username", sbsUsername);		
		requestConnection = HTTPRequest.sendPOST(SBS_AUTHENTICATE_URL, null, logoutParameters);
		if (requestConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
			String message = "deleting session at spring batch server at "
					+ sbsSessionURL
					+ " failed, response code is "
					+ requestConnection.getResponseCode();
			clearSession();
			logger.error(message);
			throw new SpringBatchClientException(message);
		}
		
		// successfully done
		clearSession();
		logger.debug("spring batch backend session ended");
		return true;
	}

	public void setSbsUsername(String sbsUsername) {
		this.sbsUsername = sbsUsername;
	}

	public void setSbsPassword(String sbsPassword) {
		this.sbsPassword = sbsPassword;
	}
	
	private void checkSession() 
			throws SpringBatchClientException {
		
		logger.debug("checking if active session exists...");

		if (sbsSessionURL == null) {
			throw new SpringBatchClientException("no active session - call startSession() first");
		}
		try {
			HttpURLConnection requestConnection;
			requestConnection = HTTPRequest.sendGET(sbsSessionURL);
			if (requestConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String message = "accessing session at spring batch server at "
						+ sbsSessionURL
						+ " failed, response code is "
						+ requestConnection.getResponseCode();
				clearSession();
				logger.error(message);
				throw new SpringBatchClientException(message);
			}
		} catch (IOException e) {
			String message = "no active session - error is " + e.getMessage();
			logger.error(message);
			throw new SpringBatchClientException(message);
		}
		
		logger.debug("active session");
		// all is well, so no exception
	}
	
	
	/**
	 * deploys a workflow to the spring batch backend
	 * 
	 * @param workflow
	 * @return true if successful, else throws exception
	 * @throws SpringBatchClientException
	 * @throws ConvertWorkflowException 
	 */
	public boolean deployWorkflow(Workflow workflow) 
			throws SpringBatchClientException, ConvertWorkflowException {
		
		logger.debug("starting deployment of workflow " + workflow.getId() + " with " +  workflow.getNumberOfDataFunctions() + " data functions");

		checkSession();

		// create workflow object following spring batch backend format
		MultiStepJob msj = Adapter.convert(workflow);

		try {
			HttpURLConnection requestConnection = HTTPRequest.sendPUT(SBS_JOBS_URL, getCookieList(), msj);
			if (requestConnection.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				String message = "deploy workflow with id " + workflow.getId()
						+ " failed, response code is "
						+ requestConnection.getResponseCode();
				logger.error(message);
				throw new SpringBatchClientException(message);
			}
		} catch (IOException e) {
			String message = "deploy workflow with id " + workflow.getId()
					+ " failed, exception is "
					+ e.getMessage();
			logger.error(message);
			throw new SpringBatchClientException(message);
		}
		
		// successfully done
		logger.debug("workflow successfully deployed at spring batch backend");
		return true;
	}

	/**
	 * executes a previously deployed workflow via the spring batch backend
	 * 
	 * @param workflow
	 * @return true if successful, else throws exception
	 * @throws SpringBatchClientException
	 */
	public boolean executeWorkflow(Workflow workflow) 
			throws SpringBatchClientException {
		
		logger.debug("starting execution of workflow with id " + workflow.getId());

		checkSession();
		
		try {			
			// trigger workflow execution at spring batch backend
			String requestUrl = SBS_JOBS_URL + "/" + Adapter.getWorkflowName(workflow.getId()) + "/run";
			HttpURLConnection requestConnection = HTTPRequest.sendPOST(requestUrl, getCookieList(), null);
			if (requestConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String message = "execute workflow with id " + workflow.getId()
						+ " failed, response code is "
						+ requestConnection.getResponseCode();
				logger.error(message);
				throw new SpringBatchClientException(message);
			}
			
			// get response
			String responseBody = HTTPRequest.readResponse(requestConnection);
			if (responseBody.isEmpty() || (responseBody.indexOf(SBS_STATUS_TAG) == -1)) {
				String message = "execute workflow with id " + workflow.getId()
						+ " failed, no execution status returned";
				logger.error(message);
				throw new SpringBatchClientException(message);
			}
			Pattern statusPattern = Pattern.compile("(" + SBS_STATUS_TAG + ")(\\s*)(:)(\\s*)(\")([a-zA-Z]+)(\")");
			Matcher statusMatcher = statusPattern.matcher(responseBody);
			String executionStatus = ""; 
			if (statusMatcher.find()) {
				executionStatus = statusMatcher.group(6);
			}
			logger.debug("execution status returned by spring batch backend: " + executionStatus);
			if (!executionStatus.equals("STARTED") && !executionStatus.equals("STARTING")) { // may be one of STARTED or STARTING
				String message = "execute workflow with id " + workflow.getId()
						+ " failed, execution status returned is "
						+ executionStatus;
				logger.error(message);
				throw new SpringBatchClientException(message);
			}			
		} catch (IOException e) {
			String message = "execute workflow with id " + workflow.getId()
					+ " failed - error is " + e.getMessage();
			logger.error(message);
			throw new SpringBatchClientException(message);
		}
		
		// successfully done
		logger.debug("workflow execution successfully started at spring batch backend");
		return true;
	}

	/**
	 * stops the execution of a previously deployed workflow via the spring batch backend
	 * 
	 * @param workflow
	 * @return true if successful, else throws exception
	 * @throws SpringBatchClientException
	 */
	public boolean stopWorkflow(Workflow workflow) 
			throws SpringBatchClientException {
		
		logger.debug("stopping execution of workflow with id " + workflow.getId());

		checkSession();

		try {			
			// stop workflow execution at spring batch backend
			String requestUrl = SBS_JOBS_URL + "/" + Adapter.getWorkflowName(workflow.getId()) + "/stop";
			HttpURLConnection requestConnection = HTTPRequest.sendPOST(requestUrl, getCookieList(), null);
			if ((requestConnection.getResponseCode() != HttpURLConnection.HTTP_OK) && (requestConnection.getResponseCode() != HttpURLConnection.HTTP_NO_CONTENT)) {
				String message = "stopping workflow with id " + workflow.getId()
						+ " failed, response code is "
						+ requestConnection.getResponseCode();
				logger.error(message);
				throw new SpringBatchClientException(message);
			}
		} catch (IOException e) {
			String message = "stopping workflow with id " + workflow.getId()
					+ " failed - error is " + e.getMessage();
			logger.error(message);
			throw new SpringBatchClientException(message);
		}

		// successfully done
		logger.debug("workflow execution successfully stopped at spring batch backend");
		return true;
	}

	/**
	 * deletes a previously deployed workflow via the spring batch backend
	 * 
	 * @param workflow
	 * @return true if successful, else throws exception
	 * @throws SpringBatchClientException
	 */
	public boolean deleteWorkflow(Workflow workflow) 
			throws SpringBatchClientException {

		logger.debug("starting deletion of workflow with id " + workflow.getId());

		checkSession();

		try {			
			// delete workflow execution at spring batch backend
			String requestUrl = SBS_JOBS_URL + "/" + Adapter.getWorkflowName(workflow.getId());
			HttpURLConnection requestConnection = HTTPRequest.sendDELETE(requestUrl, getCookieList());
			if (requestConnection.getResponseCode() != HttpURLConnection.HTTP_NO_CONTENT) {
				String message = "deleting workflow with id " + workflow.getId()
						+ " failed, response code is "
						+ requestConnection.getResponseCode();
				logger.error(message);
				throw new SpringBatchClientException(message);
			}
		} catch (IOException e) {
			String message = "deleting workflow with id " + workflow.getId()
					+ " failed - error is " + e.getMessage();
			logger.error(message);
			throw new SpringBatchClientException(message);
		}

		// successfully done
		logger.debug("workflow successfully deleted at spring batch backend");
		return true;
	}
	
	/**
	 * queries the current execution status of the given workflow at the spring
	 * batch backend and updates the wokflow's Status
	 * 
	 * @param workflow
	 * @return true if the workflow status has changed, else false
	 * @throws SpringBatchClientException
	 */
	public boolean checkStatus(Workflow workflow) 
		throws SpringBatchClientException {

		logger.debug("check status of workflow: " + workflow);

		checkSession();

		boolean statusChange = false;
		Status oldStatus = Status.getType(workflow.getStatus().getId());
		Status newStatus;
		
		try {
			// retrieve workflow status from springbatch backend
			String requestUrl = SBS_JOBS_URL + "/" + Adapter.getWorkflowName(workflow.getId());
			HttpURLConnection requestConnection = HTTPRequest.sendGET(requestUrl, getCookieList());
			if (requestConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String message = "check status of workflow with id " + workflow.getId()
						+ " failed, response code is "
						+ requestConnection.getResponseCode();
				logger.error(message);
				throw new SpringBatchClientException(message);
			}
			
			// get response
			String responseBody = HTTPRequest.readResponse(requestConnection);
			if (responseBody.isEmpty()) {
				String message = "check status of workflow with id " + workflow.getId()
						+ " failed, no execution status returned";
				logger.error(message);
				throw new SpringBatchClientException(message);
			}
			
			String registrationBody = responseBody.substring(responseBody.indexOf("{ \"job\"") + 10,
					responseBody.indexOf("}, \"executions\":") + 1);
			String jobExecutionsBody = responseBody.substring(responseBody.indexOf("}, \"executions\":") + 16,
					responseBody.length() - 1);

			ObjectMapper mapper = new ObjectMapper();
			com.ontos.ldiw.workflow.beans.JobExecution[] jobExecutions = mapper.readValue(jobExecutionsBody, com.ontos.ldiw.workflow.beans.JobExecution[].class);
			
			// here we take the last entry in the execution list
			// status field encoding see: http://docs.spring.io/spring-batch/trunk/apidocs/org/springframework/batch/core/BatchStatus.html
			String statusRetrieved = jobExecutions[jobExecutions.length - 1].getStatus();
			if (statusRetrieved.equalsIgnoreCase("STARTING") || statusRetrieved.equalsIgnoreCase("STARTED")) {
				newStatus = Status.RUNNING;
			} else if (statusRetrieved.equalsIgnoreCase("STOPPING") || statusRetrieved.equalsIgnoreCase("STOPPED")) {
				newStatus = Status.STOPPED;
			} if (statusRetrieved.equalsIgnoreCase("FAILED")) {
				newStatus = Status.FAILED;
			} if (statusRetrieved.equalsIgnoreCase("COMPLETED")) {
				newStatus = Status.COMPLETED;
			} else { // UNKNOWN or ABANDONED
				newStatus = Status.FAILED;
			}
				
			if (!newStatus.equals(oldStatus)) {
				statusChange = true;
				workflow.setStatus(newStatus);
				logger.debug("status of workflow has changed: " + workflow);
			}
			
//			for (int i = 0; i < jobExecutions.length; ++i) {
//				// TODO here we may want to update our workflow execution history
//				logger.debug("status " + jobExecutions[i].getStatus());
//			}
			
		} catch (IOException e) {
			String message = "checking status of workflow with id " + workflow.getId()
					+ " failed - error is " + e.getMessage();
			logger.error(message);
			throw new SpringBatchClientException(message);
		}
		
		return statusChange;
	}
}
