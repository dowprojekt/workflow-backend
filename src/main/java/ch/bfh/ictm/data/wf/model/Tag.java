/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * user-generated tags that may be attached to WorkflowObjects (i.e.,
 * DataFunction and Workflow). Tags may form a hierarchy (taxonomy) as each tag
 * may reference a parent tag (with NULL as default = no parent).
 * 
 * @author vgj1
 */
@Entity
@Access(value=AccessType.FIELD)
public class Tag extends MetadataObject {
	@ManyToOne(fetch = FetchType.EAGER) // JPA: not cascading since changes are managed manually
	private Tag parent;
	
	private String label = "";

	public Tag() {
	}
	
	public Tag(String label) {
		this.parent = null;
		this.label = label;
	}
	
	public boolean hasParent() {
		return (parent != null);
	}
	
	public Tag getParent() {
		return parent;
	}

	public void setParent(Tag parent) {
		this.parent = parent;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public boolean equals(Tag tag) {
		if ((tag == null) || (tag.getLabel() == null)) return false;
		return (this.label.equals(tag.getLabel()));
	}
	
	public boolean equals(String label) {
		if (label == null) return false;
		return (this.label.equals(label));
	}
	
	public String toString() {
		return label;
	}
}
