/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

/**
 * Root super class for common properties of all model objects 
 * 
 * @author vgj1
 */

// Jackson JSON Serialization - is inherited by all subclasses
@JsonAutoDetect(fieldVisibility=Visibility.ANY, getterVisibility=Visibility.NONE, isGetterVisibility=Visibility.NONE)

// JPA
@MappedSuperclass
@Access(value = AccessType.FIELD)
public abstract class ModelObject {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected long id;
	
	protected long sn = 0;

	public ModelObject() {
	}

	public long getSn() {
		return sn;
	}

	public void setSn(long sn) {
		this.sn = sn;
	}

	public long getId() {
		return id;
	}
	
	public boolean equals(long id) {
		return (this.id == id);
	}
	
	public void resetId() {
		id = 0;
	}
	
	/**
	 * is called when updating an existing model object with new data from the
	 * WFE frontend. needs to be implemented by the subclasses.
	 * 
	 * @param update the data received from the WFE frontend
	 */
	public void update(ModelObject update) {
	}
}
