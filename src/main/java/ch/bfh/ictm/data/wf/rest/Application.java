/**
 * Copyright 2015 Bern University of Applied Sciences, Switzerland
 * http://www.bfh.ch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.bfh.ictm.data.wf.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Spring Boot Web application with a RESTful API (resources encoded as JSON).
 * See Spring Boot homepage {@link http://projects.spring.io/spring-boot/} 
 * or Spring Boot Reference Manual (
 * {@link http://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/htmlsingle/}) for details.
 * 
 * @author vgj1
 *
 */

// general spring bean annotations
@Configuration
@ComponentScan(basePackages = {""
		+ "ch.bfh.ictm.data.wf.database, "
		+ "ch.bfh.ictm.data.wf.model, "
		+ "ch.bfh.ictm.data.wf.model.adapter, "
		+ "ch.bfh.ictm.data.wf.model.manager, "
		+ "ch.bfh.ictm.data.wf.model.manager.util, "
		+ "ch.bfh.ictm.data.wf.rest, "
		+ "ch.bfh.ictm.data.wf.rest.controller, "
		+ "ch.bfh.ictm.data.wf.rest.springbatchclient, "
		+ "ch.bfh.ictm.data.wf.rest.util"
	})

// spring boot
// create spring application context automatically
@EnableAutoConfiguration
// create servlet entrance
@SpringBootApplication

// spring JPA
@EntityScan(basePackages = {""
		+ "ch.bfh.ictm.data.wf.model"
	})
@EnableJpaRepositories(basePackages = {""
		+ "ch.bfh.ictm.data.wf.database"
	})
@EnableTransactionManagement(mode=AdviceMode.ASPECTJ)
public class Application extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
        
//        System.out.println("Let's inspect the beans provided by Spring Boot:");
//
//        String[] beanNames = ctx.getBeanDefinitionNames();
//        Arrays.sort(beanNames);
//        for (String beanName : beanNames) {
//            System.out.println(beanName);
//        }
    }
}
