package com.ontos.ldiw.workflow.beans;



/**
 * Bean representing schedule information for a spring batch job that is executed with Quartz.
 * @author mvoigt
 *
 */
public class Schedule {

  //in XSD DateTime Format: YYYY-MM-DDThh:mm:ss
  private String start;
  private String end;
  private boolean intervalDay;
  private boolean intervalWeek;
  private boolean intervalMonth;
  
  public Schedule() {
    
  }

  /**
   * Constructor for the schedule bean with all properties.
   * @param start start date as xsd:DateTime string
   * @param end end date date as xsd:DateTime string
   * @param intervalDay flag if the job should be scheduled daily
   * @param intervalWeek flag if the job should be scheduled weekly
   * @param intervalMonth flag if the job should be scheduled monthly
   */
  public Schedule(String start, String end,
      boolean intervalDay, boolean intervalWeek, boolean intervalMonth) {
    super();
    this.start = start;
    this.end = end;
    this.intervalDay = intervalDay;
    this.intervalWeek = intervalWeek;
    this.intervalMonth = intervalMonth;
  }

  public String getStart() {
    return start;
  }

  public void setStart(String start) {
    this.start = start;
  }

  public String getEnd() {
    return end;
  }

  public void setEnd(String end) {
    this.end = end;
  }

  public boolean isIntervalDay() {
    return intervalDay;
  }

  public void setIntervalDay(boolean intervalDay) {
    this.intervalDay = intervalDay;
  }

  public boolean isIntervalWeek() {
    return intervalWeek;
  }

  public void setIntervalWeek(boolean intervalWeek) {
    this.intervalWeek = intervalWeek;
  }

  public boolean isIntervalMonth() {
    return intervalMonth;
  }

  public void setIntervalMonth(boolean intervalMonth) {
    this.intervalMonth = intervalMonth;
  }

  
  
}
