package com.ontos.ldiw.workflow.beans;

public class JobWraper {

    private Registration job;

    public Registration getJob() {
        return job;
    }

    public void setJob(Registration job) {
        this.job = job;
    }

}
