package com.ontos.ldiw.workflow.beans;

public class JobInstanceWrapper {
  
  private JobInstance jobInstance;

  public JobInstanceWrapper() {}
  
  public JobInstance getJobInstance() {
    return jobInstance;
  }

  public void setJobInstance(JobInstance jobInstance) {
    this.jobInstance = jobInstance;
  }
  
  

}
