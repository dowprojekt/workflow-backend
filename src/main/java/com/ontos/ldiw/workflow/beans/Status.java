package com.ontos.ldiw.workflow.beans;

public enum Status {
    ABANDONED, COMPLETED, FAILED, STARTED, STARTING, STOPPED, STOPPING, UNKNOWN
}
